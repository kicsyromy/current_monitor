#include "statistics.hh"
#include "config/nvs_proxy.hh"

#include <gsl/gsl>

using namespace statistics;

namespace
{
    constexpr double EPSILON{ 4.94065645841247E-324 };

    template<typename Data, typename Number>
    constexpr auto find_threshold_cross(const gsl::span<const Data> &samples,
                                        Number                       threshold) noexcept
    {
        static_assert(std::is_integral_v<Number> || std::is_floating_point_v<Number>);
        static_assert(std::is_convertible_v<Data, Number>);

        auto previous_sample = static_cast<Number>(samples[0]) - threshold;

        for (std::size_t i = 1; i < samples.size(); ++i)
        {
            const auto current_sample = static_cast<Number>(samples[i]) - threshold;

            if ((previous_sample <= 0 && current_sample > 0) ||
                (previous_sample >= 0 && current_sample < 0))
            {
                return i;
            }

            previous_sample = current_sample;
        }

        return std::numeric_limits<std::size_t>::max();
    }

} // namespace

Statistics Statistics::compute_raw(const std::vector<std::uint16_t> &snapshot,
                                   const Statistics::Config         &config) noexcept
{
    const auto pulse_threshold{ config.pulse_threshold };
    const auto pulse_length{ config.pulse_length }; // The minimum number of samples that would
                                                    // constitute a pulse

    auto result = Statistics{};

    CM_LOG_INFO_F("PT: {} - PL: {} - SS: {} - SP: {}",
                  config.pulse_threshold,
                  config.pulse_length,
                  snapshot.size(),
                  static_cast<const void *>(snapshot.data()));
    const auto span = gsl::span{ snapshot.data(), snapshot.size() };

    auto pulse_amplitude_sum = 0.;
    auto pulse_length_sum    = 0.;

    CM_LOG_INFO_F("<><><><><> MAX: {}", *std::max_element(snapshot.cbegin(), snapshot.cend()));
    CM_LOG_INFO_F("<><><><><> MIN: {}", *std::min_element(snapshot.cbegin(), snapshot.cend()));

    for (std::size_t i = 0; i < snapshot.size() - 1;)
    {
        CM_LOG_DEBUG_F("i: {} - v: {}", i, snapshot[i]);

        auto pulse_start = find_threshold_cross(span.subspan(i), pulse_threshold);
        if (pulse_start == std::numeric_limits<std::size_t>::max())
        {
            break;
        }

        pulse_start += i;
        if (static_cast<std::uint16_t>(span[pulse_start]) < pulse_threshold)
        {
            i = pulse_start;
            continue;
        }

        auto       cross_end = find_threshold_cross(span.subspan(pulse_start), pulse_threshold);
        const auto ignore_outlier = cross_end <= pulse_length;
        if (ignore_outlier)
        {
            i = pulse_start + cross_end;
            continue;
        }
        auto pulse_end = cross_end;

        if (cross_end != std::numeric_limits<std::size_t>::max())
        {
            cross_end += pulse_start;

            // Make sure we don't count threshold values
            pulse_end += pulse_start - 1;
            for (;
                 std::abs(static_cast<std::uint16_t>(span[pulse_end]) - pulse_threshold) < EPSILON;
                 --pulse_end)
            {
            }

            // We found the end of the pulse, go one past it to make the algo and math below check
            // out
            ++pulse_end;
        }
        else
        {
            pulse_end = span.size();
            cross_end = span.size();
        }

        // printf("++++++ %u: %u -> %u", cross_end, pulse_start, pulse_end);
        // for (const auto &v : snapshot)
        // {
        //     printf(" %u", v);
        // }
        // printf(" ]\n");
        ++result.pulse_count_;
        pulse_amplitude_sum +=
            std::accumulate(&span[pulse_start],
                            &span[pulse_end >= span.size() ? span.size() - 1 : pulse_end],
                            0.,
                            [](double sum, std::uint16_t value) noexcept {
                                return sum + static_cast<double>(value);
                            });
        pulse_length_sum += static_cast<double>(pulse_end - pulse_start);

        i = cross_end;
    }

    result.pulse_amplitude_average_ =
        pulse_length_sum > 0 ? static_cast<std::uint16_t>((pulse_amplitude_sum / pulse_length_sum))
                             : 0x8000; // ADC Mid value = Zero.
    result.pulse_width_average_ =
        result.pulse_count_ > 0 ? static_cast<std::uint8_t>(pulse_length_sum / result.pulse_count_)
                                : 0;

    CM_LOG_INFO_F("Pulse Count: {}", result.pulse_count_);
    CM_LOG_INFO_F("Pulse Avg Amplitude: {}", result.pulse_amplitude_average_);
    CM_LOG_INFO_F("Pulse Avg Width: {}", result.pulse_width_average_);

    return result;
}

bool Statistics::Config::load(config::nvs::Proxy &proxy)
{
    auto esp_err = proxy.load("stputh", pulse_threshold);
    if (esp_err != ESP_OK)
    {
        CM_LOG_WARN("Failed to load pulse threshold for statistics config from nvs");
        return false;
    }

    esp_err = proxy.load("stpule", pulse_length);
    if (esp_err != ESP_OK)
    {
        CM_LOG_WARN("Failed to load pulse length for statistics config from nvs");
        return false;
    }

    return true;
}

bool Statistics::Config::save(config::nvs::Proxy &proxy)
{
    auto esp_err = proxy.save("stputh", pulse_threshold);
    if (esp_err != ESP_OK)
    {
        CM_LOG_WARN("Failed to save pulse threshold for statistics config to nvs");
        return false;
    }

    esp_err = proxy.save("stpule", pulse_length);
    if (esp_err != ESP_OK)
    {
        CM_LOG_WARN("Failed to save pulse length for statistics config to nvs");
        return false;
    }

    esp_err = proxy.commit();
    if (esp_err != ESP_OK)
    {
        CM_LOG_WARN("Failed to commit statistics config changes");
        return false;
    }
    return true;
}

Calculator::Calculator(config::nvs::Proxy     &nvs_proxy,
                       config::RemoteReceiver &remote_config_receiver)
  : _nvs_proxy(nvs_proxy)
  , _remote_cfg_receiver(remote_config_receiver)
{
    Statistics::Config config;
    if (config.load(_nvs_proxy))
    {
        CM_LOG_INFO("Loaded statistics config from nvs.");
        set_config(config);
    }
    else
    {
        CM_LOG_INFO("Loaded statistics default config.");
    }

    _remote_cfg_receiver.register_decoder(
        config::RemoteReceiver::MessageType::StatisticsCalculator,
        [this](std::string_view message) -> bool {
            if (message.size() != sizeof(Statistics::Config::Data))
            {
                CM_LOG_ERROR("Received config for statistics is too small");
                return false;
            }
            CM_LOG_INFO("Received new config for statistics calculator");

            Statistics::Config::Data cfg_data;
            memcpy(&cfg_data, message.data(), sizeof(cfg_data));

            Statistics::Config new_cfg(cfg_data);
            set_config(new_cfg);
            new_cfg.save(_nvs_proxy);

            return true;
        });
}

Calculator::~Calculator()
{
    _remote_cfg_receiver.unregister_decoder(
        config::RemoteReceiver::MessageType::StatisticsCalculator);
}

void Calculator::config_changed(const Statistics::Config &config)
{
    CM_LOG_INFO_F("Statistics calculator config changed pulse_threshold: {} pulse_length: {}",
                  config.pulse_threshold,
                  config.pulse_length);
}