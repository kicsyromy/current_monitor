#ifndef CONFIG_CM_SIMULATE_CURRENT_READINGS
#include "current_data_reader.hh"
#include "current_reader_constants.hh"
#include "utils.hh"

namespace utils
{
    namespace hw
    {
        template<gpio_num_t gpio_pin, std::uint32_t on_level = 1, std::uint32_t off_level = 0>
        struct gpio_level_hold_t
        {
            gpio_level_hold_t()
            {
                gpio_set_level(gpio_pin, on_level);
                wait_level_change();
            }

            void wait_level_change()
            {
                delay_microseconds(constants::gpio::WAIT_INTERVAL_MICROSECONDS);
            }

            ~gpio_level_hold_t()
            {
                gpio_set_level(gpio_pin, off_level);
            }
        };

        static void configure_gpio(gpio_int_type_t intr,
                                   gpio_mode_t     mode,
                                   std::uint64_t   pin_mask,
                                   gpio_pulldown_t pulldown,
                                   gpio_pullup_t   pullup)
        {
            gpio_config_t io_conf;

            io_conf.intr_type    = intr;
            io_conf.mode         = mode;
            io_conf.pin_bit_mask = pin_mask;
            io_conf.pull_down_en = pulldown;
            io_conf.pull_up_en   = pullup;
            gpio_config(&io_conf);
        }

        static inline spi_transaction_t make_spi_transaction(uint32_t flags, std::size_t length)
        {
            spi_transaction_t transaction;
            clear(transaction);

            transaction.length = length;
            transaction.flags  = flags;

            return transaction;
        }

        static void init_spi_bus(spi_host_device_t host_dev,
                                 spi_common_dma_t  spi_dma,
                                 int               miso_pin,
                                 int               mosi_pin,
                                 int               clk_pin,
                                 int               max_transfer_size)
        {
            spi_bus_config_t bus_cfg;
            clear(bus_cfg);

            bus_cfg.miso_io_num     = miso_pin;
            bus_cfg.mosi_io_num     = mosi_pin;
            bus_cfg.sclk_io_num     = clk_pin;
            bus_cfg.quadwp_io_num   = constants::SPI_UNUSED_FIELD;
            bus_cfg.quadhd_io_num   = constants::SPI_UNUSED_FIELD;
            bus_cfg.max_transfer_sz = max_transfer_size;

            esp_err_t ret = spi_bus_initialize(host_dev, &bus_cfg, spi_dma);
            ESP_ERROR_CHECK(ret);
        }

        static spi_device_handle_t configure_spi_device(spi_host_device_t host_dev,
                                                        uint8_t           mode,
                                                        int               cs_pin,
                                                        int               queue_size,
                                                        int               clock_speed_hz)
        {
            spi_device_handle_t spi_dev;

            spi_device_interface_config_t device_config;
            clear(device_config);

            device_config.mode           = mode;
            device_config.spics_io_num   = cs_pin;
            device_config.queue_size     = queue_size;
            device_config.clock_speed_hz = clock_speed_hz;

            esp_err_t ret = spi_bus_add_device(host_dev, &device_config, &spi_dev);
            ESP_ERROR_CHECK(ret);

            return spi_dev;
        }
    } // namespace hw
} // namespace utils

using namespace current_data;

struct SpiReader::Device
{
public:
    Device(spi_device_handle_t spi_dev)
      : spi(spi_dev)
    {}
    ~Device() = default;

public:
    spi_device_handle_t spi;
};

SpiReader::SpiReader(SampleBuffer<std::uint16_t> &aggregator)
  : ReaderBase(aggregator)
{
    CM_LOG_INFO("Using SPI reader");
}

SpiReader::~SpiReader() = default;

void SpiReader::setup_devices()
{
    utils::hw::configure_gpio(constants::gpio::INTR_TYPE,
                              constants::gpio::MODE,
                              constants::gpio::PIN_BIT_MASK,
                              constants::gpio::PULLDOWN,
                              constants::gpio::PULLUP);

    utils::hw::init_spi_bus(constants::SPI_DEVICE,
                            constants::spi_bus::DMA_MODE,
                            constants::spi_bus::PIN_NUM_MISO,
                            constants::spi_bus::PIN_NUM_MOSI,
                            constants::spi_bus::PIN_NUM_CLK,
                            constants::spi_bus::MAX_TRANSFER_SIZE);

    _reader_device = std::make_unique<Device>(
        utils::hw::configure_spi_device(constants::SPI_DEVICE,
                                        constants::spi_dev::MODE,
                                        constants::spi_dev::CS_PIN,
                                        constants::spi_dev::QUEUE_SIZE,
                                        constants::spi_dev::CLOCK_SPEED_HZ));
}

bool SpiReader::read_data(std::uint16_t &out_data)
{
    utils::hw::gpio_level_hold_t<GPIO_NUM_5> gpio_level;

    auto transaction{ utils::hw::make_spi_transaction(constants::transaction::FLAGS,
                                                      constants::transaction::DATA_LENGTH_BITS) };

    const auto ret{ spi_device_polling_transmit(_reader_device->spi, &transaction) };
    ESP_ERROR_CHECK(ret);

    const auto has_data{ transaction.rxlength > 0 };
    if (has_data)
    {
        out_data =
            std::uint16_t(transaction.rx_data[0] << 8);
        out_data = std::uint16_t(out_data | transaction.rx_data[1]);
//        CM_LOG_INFO_F("{}", out_data);
    }

    return has_data;
}

SpiReader current_data::make_reader(SampleBuffer<std::uint16_t> &aggregator)
{
    return SpiReader(aggregator);
}

#endif
