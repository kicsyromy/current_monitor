#ifdef __xtensa__

#include "common.hh"
#include "config/nvs_proxy.hh"
#include "config/remote_receiver.hh"
#include "current_data_reader.hh"
#include "data_publisher.hh"
#include "logger.hh"
#include "mqtt_client.hh"
#include "ota.hh"
#include "resources.hh"
#include "sample_buffer.hh"
#include "statistics.hh"
#include "wifi.hh"

#include <esp_event.h>
#include <esp_tls.h>
#include <nvs_flash.h>

#include <optional>
#include <string_view>

namespace
{
    inline void initialize_nvs()
    {
        auto ret = nvs_flash_init();
        if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
        {
            ESP_ERROR_CHECK(nvs_flash_erase());
            ret = nvs_flash_init();
        }
        ESP_ERROR_CHECK(ret);
    }

    inline void initialize_tcp_ip_stack()
    {
        ESP_ERROR_CHECK(esp_netif_init());
    }

    inline void create_main_event_loop()
    {
        ESP_ERROR_CHECK(esp_event_loop_create_default());
    }

    inline void initialize_wifi()
    {
        esp_netif_create_default_wifi_sta();
        const wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
        ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    }

    inline void initialize_spi()
    {}
} // namespace

extern "C" void app_main()
{
    initialize_nvs();

    esp_err_t esp_ret = ESP_FAIL;
    esp_ret           = esp_tls_set_global_ca_store(
        CERT_ISRG_X1_START,
        static_cast<std::uint32_t>(CERT_ISRG_X1_END - CERT_ISRG_X1_START));
    if (esp_ret != ESP_OK)
    {
        CM_LOG_ERROR_F("Error in setting the global ca store: [{:x}] ({}),could not complete the "
                       "https_request using global_ca_store",
                       esp_ret,
                       esp_err_to_name(esp_ret));
    }

    initialize_tcp_ip_stack();
    create_main_event_loop();
    initialize_wifi();

    static auto wifi = WiFi{};
    CM_LOG_INFO_F("Connecting to WiFi access point {} with password {}...",
                  WIFI_SSID,
                  WIFI_PASSWORD);

    auto wifi_connected = std::optional<bool>{ std::nullopt };

    auto on_wifi_connected = [&wifi_connected](WiFi *, std::uint32_t ip_address) {
        CM_LOG_INFO_F("Connected to access point with SSID: \"{}\" and password: \"{}\"",
                      WIFI_SSID,
                      WIFI_PASSWORD);

        CM_LOG_INFO_F("Ip address: {}.{}.{}.{}",
                      (ip_address >> 0) & 0xFF,
                      (ip_address >> 8) & 0xFF,
                      (ip_address >> 16) & 0xFF,
                      (ip_address >> 24) & 0xFF);

        wifi_connected = true;
    };
    wifi.connected.connect(on_wifi_connected);

    auto on_wifi_connection_failed = [&wifi_connected](WiFi *) {
        CM_LOG_ERROR("Failed to connect to Access Point");

        wifi_connected = false;

        CM_LOG_INFO("Restarting...");
        esp_restart();
    };
    wifi.connection_failed.connect(on_wifi_connection_failed);

    auto on_wifi_disconnected = [&wifi_connected](WiFi *) {
        CM_LOG_INFO("WiFi disconnected");

        wifi_connected = false;
    };
    wifi.disconnected.connect(on_wifi_disconnected);

    wifi.set_ssid(WIFI_SSID);
    wifi.set_password(WIFI_PASSWORD);
    wifi.set_authentication(WiFi::AuthenticationMode::WPA2_PSK);
    wifi.connect();

    while (wifi_connected == std::nullopt)
    {
        utils::delay(std::chrono::milliseconds(100));
    }

    CM_LOG_INFO_F("OTA: Firmware server URI {}", CONFIG_CM_CONFIG_OTA_URI);
    ota::try_ota_update(CONFIG_CM_CONFIG_OTA_URI, OTA_SERVER_CERT_START);

    static auto mqtt_client = MQTTClient{};

    static auto nvs_proxy              = config::nvs::Proxy{ "currentmonitor" };
    static auto remote_config_receiver = config::RemoteReceiver{};
    remote_config_receiver.register_decoder(
        config::RemoteReceiver::MessageType::FirmwareUpdate,
        [](std::string_view message) noexcept {
            if (message.size() == 1 && message.data()[0] == static_cast<char>(0xDE))
            {
                ota::try_ota_update(CONFIG_CM_CONFIG_OTA_URI, OTA_SERVER_CERT_START);
            }
        });

    static auto aggregator = SampleBuffer<std::uint16_t>{};
    aggregator.resize(1000);

    static auto stats_calculator = statistics::Calculator{ nvs_proxy, remote_config_receiver };


    // FIXME: But whyyyy??!
    sleep(1);

    mqtt_client.set_broker_url(MQTT_BROKER_URL, MQTT_TLS_PORT);
    mqtt_client.set_username(MQTT_USERNAME);
    mqtt_client.set_password(MQTT_PASSWORD);

    static auto reader = current_data::make_reader(aggregator);
    static auto data_publisher =
        DataPublisher{ nvs_proxy,
                       remote_config_receiver,
                       stats_calculator,
                       reader,
                       aggregator,
                       [](std::string_view topic, std::string_view message) {
                              mqtt_client.publish_message(topic, message);
                       } };

    std::atomic_bool mqtt_connected;
    auto             on_mqtt_connected = [&mqtt_connected](void *sender) {
        CM_LOG_INFO_F("MQTT: Connected to {}...", MQTT_BROKER_URL);

        auto mqtt_client_instance = static_cast<MQTTClient *>(sender);
        mqtt_client_instance->publish_message(CONFIG_CM_RESET_PUBLISH_TOPIC, CONFIG_CM_VERSION);
        mqtt_connected.store(true, std::memory_order_release);
        reader.start_reading();
    };

    mqtt_client.connected.connect(on_mqtt_connected);

    auto on_mqtt_message_published = [](void *, MQTTClient::MessageID message_id) {
        CM_LOG_INFO_F("MQTT: Message {} published...", message_id);
    };

    mqtt_client.published.connect(on_mqtt_message_published);

    auto on_mqtt_message_received =
        [](MQTTClient *self, std::string_view topic, const std::string &data) {
            CM_LOG_INFO_F("MQTT received message on topic {}", topic);
            if (topic == CONFIG_CM_CONFIG_REQUEST_TOPIC)
            {
                const auto response = remote_config_receiver.message_received(data);
                self->publish_message(CONFIG_CM_CONFIG_RESPONSE_TOPIC, response);
            }
        };
    mqtt_client.received.connect(on_mqtt_message_received);

    CM_LOG_INFO("Connecting to MQTT broker...");
    mqtt_client.connect();

    while (mqtt_connected.load(std::memory_order_acquire) == false)
    {
        utils::delay(std::chrono::milliseconds(100));
    }

    CM_LOG_INFO_F("MQTT subscribed to topic {}", CONFIG_CM_CONFIG_REQUEST_TOPIC);
    mqtt_client.subscribe(CONFIG_CM_CONFIG_REQUEST_TOPIC);
    data_publisher.run();
}

#endif /* __xtensa__ */
