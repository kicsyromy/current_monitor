#include "data_message.hh"

#include <cstring>

static constexpr std::size_t HEADER_RESERVED_SIZE{ 2 };

DataMessage::DataMessage()
  : _message(HEADER_RESERVED_SIZE, '_')
{}

void DataMessage::set_reset(DataMessage::Reset reset)
{
    if (_reset != reset)
    {
        _reset = reset;
    }
}

void DataMessage::set_status(DataMessage::Status status)
{
    if (_status != status)
    {
        _status = status;
    }
}

void DataMessage::reset()
{
    _message.clear();
    _message.resize(HEADER_RESERVED_SIZE, '_');
}

std::string_view DataMessage::serialize(const std::array<char, 2> &header)
{
    _message[0] = header[0];
    _message[1] = header[1];

    return _message;
}
