#include "ota.hh"

#include "logger.hh"

#include <fmt/format.h>

#include <esp_https_ota.h>
#include <esp_ota_ops.h>

#include <charconv>
#include <cstring>

OtaHandler::OtaHandler(ota::Config &&config) noexcept
  : ota_url_{ config.url }
  , ota_server_certificate_{ config.ota_server_certificate }
{}

ota::Error OtaHandler::establish_connection() noexcept
{
    esp_http_client_config_t http_config{};
    http_config.user_data         = this;
    http_config.url               = ota_url_.c_str();
    http_config.cert_pem          = reinterpret_cast<const char *>(ota_server_certificate_);
    http_config.keep_alive_enable = true;
    http_config.event_handler     = [](esp_http_client_event_t *event) noexcept {
        auto *self = static_cast<OtaHandler *>(event->user_data);
        switch (event->event_id)
        {
        case HTTP_EVENT_ERROR:
            self->error.fire(self);
            break;
        case HTTP_EVENT_ON_CONNECTED:
            self->connected.fire(self);
            break;
        case HTTP_EVENT_HEADER_SENT:
            self->headers_sent.fire(self);
            break;
        case HTTP_EVENT_ON_HEADER:
            self->header_received.fire(self, event->header_key, event->header_value);
            break;
        case HTTP_EVENT_ON_DATA: {
            auto data = std::vector<std::uint8_t>(static_cast<std::size_t>(event->data_len),
                                                  std::uint8_t{ 0 });
            std::memcpy(data.data(), event->data, static_cast<std::size_t>(event->data_len));
            self->data_received.fire(self, std::move(data));

            break;
        }
        case HTTP_EVENT_ON_FINISH:
            self->finished.fire(self);
            break;
        case HTTP_EVENT_DISCONNECTED:
            self->disconnected.fire(self);
            break;
        }
        return ESP_OK;
    };

    auto ota_config        = esp_https_ota_config_t{};
    ota_config.http_config = &http_config;

    switch (esp_https_ota_begin(&ota_config, &ota_handle_))
    {
    case ESP_OK:
        connection_established_ = true;
        return ota::Error::Ok;
    case ESP_ERR_INVALID_ARG:
        return ota::Error::InvalidArgument;
    }

    return ota::Error::Unknown;
}

ota::Error OtaHandler::close_connection()
{
    auto status = esp_https_ota_abort(ota_handle_);
    if (status == ESP_OK)
    {
        ota_handle_ = nullptr;
        return ota::Error::Ok;
    }

    return ota::Error::Unknown;
}

std::pair<bool, ota::Error> OtaHandler::is_update_available() const
{
    if (!connection_established_)
    {
        return { false, ota::Error::NoConnection };
    }

    auto image_info = esp_app_desc_t{};
    switch (esp_https_ota_get_img_desc(ota_handle_, &image_info))
    {
    case ESP_OK:
        break;
    case ESP_ERR_INVALID_ARG:
        return { false, ota::Error::NoConnection };
    case ESP_FAIL:
        return { false, ota::Error::ImageInfoReadFailed };
    default:
        return { false, ota::Error::Unknown };
    }

    const auto &current_app_sha256   = esp_ota_get_app_description()->app_elf_sha256;
    auto        current_image_sha256 = std::array<char, 64 + 1>{};
    for (std::size_t i = 0; i < 32; ++i)
    {
        fmt::format_to(current_image_sha256.begin() + i, "{:02x}", current_app_sha256[i]);
    }

    auto update_sha256 = std::array<char, 64 + 1>{};
    for (std::size_t i = 0; i < 32; ++i)
    {
        fmt::format_to(update_sha256.begin() + i, "{:02x}", image_info.app_elf_sha256[i]);
    }

    CM_LOG_INFO_F("\n***************************************************"
                  "\n  Active: {}"
                  "\n  OTA: {}"
                  "\n  Update compile time: {}"
                  "\n  Update compile date: {}"
                  "\n***************************************************",
                  current_image_sha256.data(),
                  update_sha256.data(),
                  image_info.time,
                  image_info.date);

    return { std::memcmp(current_app_sha256, image_info.app_elf_sha256, 32) != 0, ota::Error::Ok };
}

ota::Error OtaHandler::execute_ota_update()
{
    auto ota_status = ESP_ERR_HTTPS_OTA_IN_PROGRESS;
    while (ota_status == ESP_ERR_HTTPS_OTA_IN_PROGRESS)
    {
        ota_status = esp_https_ota_perform(ota_handle_);
        std::printf(".");
    }
    std::puts("");

    ota_status = ota_status == ESP_OK ? esp_https_ota_finish(ota_handle_) : ota_status;
    if (ota_status == ESP_OK)
    {
        ota_handle_ = nullptr;
        return ota::Error::Ok;
    }
    else
    {
        using ota::Error;
        switch (ota_status)
        {
        case ESP_ERR_INVALID_ARG:
            return Error::InvalidArgument;
        case ESP_ERR_OTA_VALIDATE_FAILED:
            return Error::ValidateFailed;
        case ESP_ERR_NO_MEM:
            return Error::AllocationFailed;
        case ESP_ERR_FLASH_OP_TIMEOUT:
        case ESP_ERR_FLASH_OP_FAIL:
            return Error::FlashError;
        }
    }

    return ota::Error::Unknown;
}

std::pair<ota::TimePoint, ota::Error> OtaHandler::update_compiled_at() const
{
    if (!connection_established_)
    {
        return { {}, ota::Error::NoConnection };
    }

    auto image_info = esp_app_desc_t{};
    switch (esp_https_ota_get_img_desc(ota_handle_, &image_info))
    {
    case ESP_OK:
        break;
    case ESP_ERR_INVALID_ARG:
        return { {}, ota::Error::NoConnection };
    case ESP_FAIL:
        return { {}, ota::Error::ImageInfoReadFailed };
    default:
        return { {}, ota::Error::Unknown };
    }

    std::string_view time{ image_info.time };
    std::int32_t     hours{};
    std::int32_t     minutes{};
    std::int32_t     seconds{};

    auto separator_position = time.find_first_of(':');
    for (std::size_t i = 0; i < 3; ++i)
    {
        if (i != 2 && separator_position == std::string_view::npos)
        {
            break;
        }

        auto slice = time.substr(0, separator_position);

        std::uint8_t value{};
        std::from_chars(slice.data(), slice.data() + slice.size(), value);

        switch (i)
        {
        case 0:
            hours = value;
            break;
        case 1:
            minutes = value;
            break;
        case 2:
            seconds = value;
            break;
        }

        time               = time.substr(separator_position + 1);
        separator_position = time.find_first_of(':');
    }

    std::string_view date{ image_info.date };
    std::int32_t     month{};
    std::int32_t     day{};
    std::int32_t     year{};

    separator_position = date.find_first_of(' ');
    for (std::size_t i = 0; i < 3; ++i)
    {
        if (i != 2 && separator_position == std::string_view::npos)
        {
            break;
        }

        auto slice = date.substr(0, separator_position);

        switch (i)
        {
        case 0:
            if (slice == std::string_view{ "Jan" })
            {
                month = 0;
            }
            else if (slice == std::string_view{ "Feb" })
            {
                month = 1;
            }
            else if (slice == std::string_view{ "Mar" })
            {
                month = 2;
            }
            else if (slice == std::string_view{ "Apr" })
            {
                month = 3;
            }
            else if (slice == std::string_view{ "May" })
            {
                month = 4;
            }
            else if (slice == std::string_view{ "Jun" })
            {
                month = 5;
            }
            else if (slice == std::string_view{ "Jul" })
            {
                month = 6;
            }
            else if (slice == std::string_view{ "Aug" })
            {
                month = 7;
            }
            else if (slice == std::string_view{ "Sep" })
            {
                month = 8;
            }
            else if (slice == std::string_view{ "Oct" })
            {
                month = 9;
            }
            else if (slice == std::string_view{ "Nov" })
            {
                month = 10;
            }
            else if (slice == std::string_view{ "Dec" })
            {
                month = 11;
            }

            break;
        case 1:
            std::from_chars(slice.data(), slice.data() + slice.size(), day);
            break;
        case 2:
            std::from_chars(slice.data(), slice.data() + slice.size(), year);
            break;
        }

        date               = date.substr(separator_position + 1);
        separator_position = date.find_first_of(' ');
    }

    auto timeinfo    = std::tm{};
    timeinfo.tm_hour = hours;
    timeinfo.tm_min  = minutes;
    timeinfo.tm_sec  = seconds;
    timeinfo.tm_year = year - 1900;
    timeinfo.tm_mon  = month;
    timeinfo.tm_mday = day;

    return { std::chrono::system_clock::from_time_t(std::mktime(&timeinfo)), ota::Error::Ok };
}
