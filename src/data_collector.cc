#include <data_collector.hh>
#include "sample_collector.hh"
#include "stats_collector.hh"
#include <data_publisher.hh>

namespace
{
    std::array<std::unique_ptr<DataCollector>(*)(DataPublisher& publisher, DataMessage& message, std::size_t count), DataCollector::CollectorType::COLLECTOR_TYPE_COUNT> COLLECTOR_ALLOCATORS
    {{
        [] (DataPublisher& publisher,DataMessage& message, std::size_t count) -> std::unique_ptr<DataCollector>
        {
            return std::make_unique<StatsCollector>(publisher, message, count);
        },
        [] (DataPublisher& publisher, DataMessage& message, std::size_t count) -> std::unique_ptr<DataCollector>
        {
            return std::make_unique<SampleCollector>(publisher, message, count);
        }
    }};
}

DataCollector::DataCollector(DataPublisher& data_publisher, DataMessage& data_message, std::size_t data_count)
    : _publisher(data_publisher)
    , _data_message(data_message)
    , _data_count(data_count)
{
}

std::unique_ptr<DataCollector> DataCollector::make_new(DataPublisher& data_publisher, DataCollector::CollectorType type, DataMessage& message, std::size_t data_count)
{
    const auto allocator_index = static_cast<std::size_t>(type);
    if (allocator_index < COLLECTOR_ALLOCATORS.size())
    {
        return COLLECTOR_ALLOCATORS[allocator_index](data_publisher, message, data_count);
    }

    return std::unique_ptr<DataCollector>(nullptr);
}