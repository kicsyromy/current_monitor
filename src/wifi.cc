#include "wifi.hh"

#include "logger.hh"

#ifdef __xtensa__

#include <cstring>

namespace
{
    constexpr auto WIFI_CONNECTED_BIT = std::uint32_t{ BIT0 };
    constexpr auto WIFI_FAIL_BIT      = std::uint32_t{ BIT1 };
} // namespace

WiFi::WiFi() noexcept
  : event_group_{ xEventGroupCreate() }
{
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));

    ESP_ERROR_CHECK(esp_event_handler_instance_register(ESP_EVENT_ANY_BASE,
                                                        ESP_EVENT_ANY_ID,
                                                        &event_handler,
                                                        this,
                                                        &event_handler_instance_));
}

WiFi::~WiFi() noexcept
{
    disconnect();

    vEventGroupDelete(event_group_);
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(ESP_EVENT_ANY_BASE,
                                                          ESP_EVENT_ANY_ID,
                                                          event_handler_instance_));
}

void WiFi::set_ssid(std::string_view ssid) noexcept
{
    constexpr std::size_t MAX_SIZE = sizeof(WiFiConfig::sta.ssid);

    const std::size_t byte_count = ssid.size() >= MAX_SIZE ? MAX_SIZE : ssid.size();
    std::memcpy(config_.sta.ssid, ssid.data(), byte_count);
}

void WiFi::set_password(std::string_view password) noexcept
{
    constexpr std::size_t MAX_SIZE = sizeof(WiFiConfig::sta.password);

    const std::size_t byte_count = password.size() >= MAX_SIZE ? MAX_SIZE : password.size();
    std::memcpy(config_.sta.password, password.data(), byte_count);
}

void WiFi::set_authentication(AuthenticationMode mode) noexcept
{
    static_assert(static_cast<int>(AuthenticationMode::Open) == WIFI_AUTH_OPEN);
    static_assert(static_cast<int>(AuthenticationMode::WEP) == WIFI_AUTH_WEP);
    static_assert(static_cast<int>(AuthenticationMode::WPA2_PSK) == WIFI_AUTH_WPA2_PSK);
    static_assert(static_cast<int>(AuthenticationMode::WPA_WPA2_PSK) == WIFI_AUTH_WPA_WPA2_PSK);
    static_assert(static_cast<int>(AuthenticationMode::WPA3_PSK) == WIFI_AUTH_WPA3_PSK);
    static_assert(static_cast<int>(AuthenticationMode::WPA2_WPA3_PSK) == WIFI_AUTH_WPA2_WPA3_PSK);

    config_.sta.threshold.authmode =
        static_cast<decltype(WiFiConfig::sta.threshold.authmode)>(mode);
}

void WiFi::connect() noexcept
{
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &config_));
    ESP_ERROR_CHECK(esp_wifi_start());

    /* Following call blocks until one of the BITs is set */
    [[maybe_unused]] EventBits_t bits = xEventGroupWaitBits(event_group_,
                                                            WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
                                                            pdFALSE,
                                                            pdFALSE,
                                                            portMAX_DELAY);
}

void WiFi::disconnect() noexcept
{
    if (is_connected_)
    {
        ongoing_retry_count_ = 0;
        ESP_ERROR_CHECK(esp_wifi_disconnect());
    }
}

void WiFi::event_handler(void        *instance,
                         ESPEventBase event_base,
                         std::int32_t event_id,
                         void        *event_data)
{
    auto *self = static_cast<WiFi *>(instance);

    if (event_base == WIFI_EVENT)
    {
        if (event_id == WIFI_EVENT_STA_START)
        {
            esp_wifi_connect();
        }
        else if (event_id == WIFI_EVENT_STA_DISCONNECTED)
        {
            if (self->is_connected_)
            {
                self->is_connected_ = false;
                self->disconnected.fire(self);
            }
            else if (self->ongoing_retry_count_ < WIFI_MAXIMUM_RETRIES)
            {
                esp_wifi_connect();
                ++(self->ongoing_retry_count_);
                CM_LOG_WARN_F("Connection failed, retrying for the {}th time",
                              self->ongoing_retry_count_);
            }
            else
            {
                xEventGroupSetBits(self->event_group_, WIFI_FAIL_BIT);
                self->connection_failed.fire(self);
            }
        }
    }
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        const auto *event          = static_cast<ip_event_got_ip_t *>(event_data);
        self->ongoing_retry_count_ = 0;
        self->is_connected_        = true;
        self->connected.fire(self, event->ip_info.ip.addr);
        xEventGroupSetBits(self->event_group_, WIFI_CONNECTED_BIT);
    }
}
#else

WiFi::WiFi() noexcept
{
    CM_UNUSED(config_);
    CM_UNUSED(event_group_);
    CM_UNUSED(event_handler_instance_);
    CM_UNUSED(ongoing_retry_count_);
    CM_UNUSED(is_connected_);
}

WiFi::~WiFi() noexcept
{}

void WiFi::set_ssid(std::string_view) noexcept
{}

void WiFi::set_password(std::string_view) noexcept
{}

void WiFi::set_authentication(AuthenticationMode) noexcept
{}

void WiFi::connect() noexcept
{}

void WiFi::disconnect() noexcept
{}

void WiFi::event_handler(void *instance, ESPEventBase, std::int32_t, void *)
{
    auto *self = static_cast<WiFi *>(instance);
    (void)self;
}

#endif /* __xtensa__ */
