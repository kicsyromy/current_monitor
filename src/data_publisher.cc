#include "data_publisher.hh"
#include "config/nvs_proxy.hh"
#include "logger.hh"

#include <esp_event.h>
#include <esp_system.h>

namespace
{
    esp_event_base_t BUFFER_FULL_EVENT_BASE = "BUFFER_FULL_EVENT_BASE";

    struct PublisherTaskCommand
    {
        std::string_view topic;
        std::string      message;
    };

    [[noreturn]] static void publisher_task(void *arg)
    {
        const auto            data_publisher = static_cast<const DataPublisher *>(arg);
        PublisherTaskCommand *command{ nullptr };
        while (true)
        {
            if (xQueueReceive(data_publisher->publisher_queue(),
                              reinterpret_cast<void *>(&command),
                              portMAX_DELAY) == pdTRUE)
            {
                if (command != nullptr)
                {
                    data_publisher->message_publisher()(command->topic, command->message);
                    delete command;
                    command = nullptr;
                }
                else
                {
                    CM_LOG_WARN("Found nullptr in queue");
                }
            }
            else
            {
                CM_LOG_INFO("Empty publish command queue.");
            }
        }
    }
} // namespace

bool DataPublisherConfig::save(config::nvs::Proxy &config_proxy)
{
    auto esp_err = config_proxy.save("dpint", interval);
    if (esp_err != ESP_OK)
    {
        CM_LOG_WARN("Failed to save data publisher interval from nvs");
        return false;
    }

    esp_err = config_proxy.save("dpdc", data_count);
    if (esp_err != ESP_OK)
    {
        CM_LOG_WARN("Failed to save data publisher data count from nvs");
        return false;
    }

    esp_err = config_proxy.save("dpcl", static_cast<std::uint8_t>(collector));
    if (esp_err != ESP_OK)
    {
        CM_LOG_WARN("Failed to save data publisher collector from nvs");
        return false;
    }

    esp_err = config_proxy.commit();
    if (esp_err != ESP_OK)
    {
        CM_LOG_WARN("Failed to commit data publisher config changes.");
        return false;
    }

    return true;
}

bool DataPublisherConfig::load(config::nvs::Proxy &config_proxy)
{
    auto esp_err = config_proxy.load("dpint", interval);
    if (esp_err != ESP_OK)
    {
        CM_LOG_WARN("Failed to load data publisher interval from nvs");
        return false;
    }

    esp_err = config_proxy.load("dpdc", data_count);
    if (esp_err != ESP_OK)
    {
        CM_LOG_WARN("Failed to load data publisher data count from nvs");
        return false;
    }

    std::uint8_t tmp_collector;
    esp_err = config_proxy.load("dpcl", tmp_collector);
    if (esp_err != ESP_OK)
    {
        CM_LOG_WARN("Failed to load data publisher collector from nvs");
        return false;
    }

    collector = static_cast<DataCollector::CollectorType>(tmp_collector);

    return true;
}

DataPublisher::DataPublisher(config::nvs::Proxy          &nvs_proxy,
                             config::RemoteReceiver      &remote_cfg_receiver,
                             statistics::Calculator      &stats_calculator,
                             current_data::SpiReader     &reader,
                             SampleBuffer<std::uint16_t> &aggregator,
                             message_publisher_t          publisher)
  : _nvs_proxy(nvs_proxy)
  , _remote_cfg_receiver(remote_cfg_receiver)
  , _stats_calculator(stats_calculator)
  , _aggregator(aggregator)
  , _reader(reader)
  , _publisher(std::move(publisher))
  , _data_message()
  , _publisher_queue(xQueueCreate(100, sizeof(PublisherTaskCommand *)))
  , _publish_header{ 0, 0 }
{
    DataPublisherConfig nvs_config;
    if (nvs_config.load(_nvs_proxy))
    {
        CM_LOG_INFO("Loaded data publisher config from nvs");
        set_config(nvs_config);
    }
    else
    {
        CM_LOG_INFO("Using default data publisher config");
        const auto &config = get_config();
        update_data_collector(config);
    }

    _remote_cfg_receiver.register_decoder(
        config::RemoteReceiver::MessageType::DataPublisher,
        [this](std::string_view message) -> bool {
            if (message.size() < sizeof(DataPublisherConfig::Data))
            {
                CM_LOG_ERROR("Received data publisher config is too small.");
                return false;
            }

            DataPublisherConfig::Data cfg_data;
            memcpy(&cfg_data, message.data(), sizeof(cfg_data));

            DataPublisherConfig new_cfg(cfg_data);
            set_config(new_cfg);
            new_cfg.save(_nvs_proxy);

            return true;
        });

    ESP_ERROR_CHECK(esp_event_handler_instance_register(
        BUFFER_FULL_EVENT_BASE,
        0,
        [](void *instance, esp_event_base_t, std::int32_t, void *) {
            auto &self = *static_cast<DataPublisher *>(instance);

            if (self._semaphore.take(portMAX_DELAY))
            {
                const auto snapshot = self._aggregator.inactive_buffer();
                self._collector->collect(snapshot);
                self.try_publish_collected();

                self._semaphore.release();
            }
            else
            {
                CM_LOG_WARN("Failed to take semaphore in data collector task loop");
            }
        },
        this,
        nullptr));

    if (xTaskCreatePinnedToCore(publisher_task, "PublisherTask", 0x1000, this, 1, nullptr, 0) !=
        pdPASS)
    {
        CM_LOG_ERROR("Failed to create publisher task");
    }
}

DataPublisher::~DataPublisher()
{
    _remote_cfg_receiver.unregister_decoder(config::RemoteReceiver::MessageType::DataPublisher);
}

[[noreturn]] void DataPublisher::run()
{
    auto on_buffer_full = [this](auto *) {
        const auto result = esp_event_post(BUFFER_FULL_EVENT_BASE, 0, nullptr, 0, 1);
        if (result != ESP_OK)
        {
            CM_LOG_ERROR_F("Failed to post on buffer full; ESP32 error: {}",
                           esp_err_to_name(result));
        }
    };
    _aggregator.buffer_full.connect(on_buffer_full);

    for (;;)
    {
        utils::delay(std::chrono::seconds{ 1 });
    }
}

void DataPublisher::try_publish_collected()
{
    if (_collector->is_filled())
    {
        send_publish_command();
        _data_message.reset();
        _collector->message_published();
    }
}

void DataPublisher::send_publish_command()
{
    PublisherTaskCommand *command = new PublisherTaskCommand();
    command->topic                = _collector->data_topic();
    command->message              = std::string(_data_message.serialize(_publish_header));
    if (xQueueSend(_publisher_queue, reinterpret_cast<void *>(&command), 0) == errQUEUE_FULL)
    {
        CM_LOG_WARN("Publish task queue full.");
    }
}

void DataPublisher::config_changed(const DataPublisherConfig &config)
{
    CM_LOG_INFO_F("Data publisher config updated interval: {}, data_count: {}, collector: {}",
                  config.interval,
                  config.data_count,
                  config.collector);
    update_data_collector(config);
}

void DataPublisher::update_data_collector(const DataPublisherConfig &config)
{
    if (_semaphore.take(portMAX_DELAY))
    {
        _collector =
            DataCollector::make_new(*this, config.collector, _data_message, config.data_count);
        if (_collector == nullptr)
        {
            CM_LOG_WARN("Failed to update data collector, defaulting...");
            const auto default_cfg = DataPublisherConfig{};
            _collector             = DataCollector::make_new(*this,
                                                 default_cfg.collector,
                                                 _data_message,
                                                 default_cfg.data_count);
            if (_collector == nullptr)
            {
                CM_LOG_ERROR("Failed to allocate default data collector, restarting...");
                esp_restart();
            }
        }

        if (config.collector == DataCollector::Sample)
        {
            _reader.set_interval(std::chrono::duration_cast<std::chrono::microseconds>(
                                     std::chrono::seconds{ config.interval }) /
                                 config.data_count);
            _aggregator.resize(config.data_count);

            const auto value = static_cast<std::uint16_t>(CONFIG_CM_DECODER_ID_WAVE);
            const auto msb   = static_cast<std::uint8_t>((value >> 8) & 0xff);
            const auto lsb   = static_cast<std::uint8_t>(value & 0xFF);

            _publish_header[0] = static_cast<char>(msb);
            _publish_header[1] = static_cast<char>(lsb);
        }
        else
        {
            const auto readings_per_sec = std::chrono::duration_cast<std::chrono::microseconds>(
                                              std::chrono::seconds{ config.interval })
                                              .count() /
                                          current_data::ReaderBase::DEFAULT_INTERVAL.count();

            _reader.set_interval(current_data::ReaderBase::DEFAULT_INTERVAL);
            _aggregator.resize(static_cast<std::size_t>(readings_per_sec));

            const auto value = static_cast<std::uint16_t>(CONFIG_CM_DECODER_ID_NORMAL);
            const auto msb   = static_cast<std::uint8_t>((value >> 8) & 0xff);
            const auto lsb   = static_cast<std::uint8_t>(value & 0xFF);

            _publish_header[0] = static_cast<char>(msb);
            _publish_header[1] = static_cast<char>(lsb);
        }

        _semaphore.release();
    }
    else
    {
        CM_LOG_WARN("Failed to set data collector due to semaphore take failure");
    }
}
