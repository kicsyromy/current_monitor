#pragma once

#include "config/configurable.hh"
#include "config/nvs_config.hh"
#include "current_data_reader.hh"
#include "data_collector.hh"
#include "data_message.hh"
#include "sample_buffer.hh"
#include "statistics.hh"
#include "utils.hh"

#include <freertos/queue.h>

#include <array>
#include <chrono>
#include <memory>

struct DataPublisherConfig final : public config::nvs::Config
{
    static constexpr std::uint8_t  DEFAULT_INTERVAL{ 1 };
    static constexpr std::uint16_t DEFAULT_DATA_COUNT{ 60 };
    static constexpr auto          DEFAULT_COLLECTOR_TYPE{ DataCollector::CollectorType::Stats };

public:
    struct PACKED Data
    {
        std::uint8_t  interval;
        std::uint16_t data_count;
        std::uint8_t  collector;
    };

public:
    DataPublisherConfig() = default;
    DataPublisherConfig(const Data &data)
      : interval(data.interval)
      , data_count(data.data_count)
      , collector(static_cast<DataCollector::CollectorType>(data.collector))
    {}

public:
    bool load(config::nvs::Proxy &) override;
    bool save(config::nvs::Proxy &) override;

public:
    std::uint8_t                 interval{ DEFAULT_INTERVAL };
    std::uint16_t                data_count{ DEFAULT_DATA_COUNT };
    DataCollector::CollectorType collector{ DEFAULT_COLLECTOR_TYPE };
};

class DataPublisher : public config::Configurable<DataPublisherConfig>
{
public:
    using message_publisher_t = void (*)(std::string_view, std::string_view);

public:
    DataPublisher(config::nvs::Proxy          &nvs_proxy,
                  config::RemoteReceiver      &remote_cfg_receiver,
                  statistics::Calculator      &stats_calculator,
                  current_data::SpiReader     &reader,
                  SampleBuffer<std::uint16_t> &aggregator,
                  message_publisher_t          publisher);
    ~DataPublisher();

public:
    [[noreturn]] void run();
    QueueHandle_t     publisher_queue() const
    {
        return _publisher_queue;
    }
    message_publisher_t message_publisher() const
    {
        return _publisher;
    }
    statistics::Calculator &stats_calculator()
    {
        return _stats_calculator;
    }

private:
    void try_publish_collected();
    void send_publish_command();
    void config_changed(const DataPublisherConfig &) override;
    void update_data_collector(const DataPublisherConfig &);

private:
    config::nvs::Proxy            &_nvs_proxy;
    config::RemoteReceiver        &_remote_cfg_receiver;
    statistics::Calculator        &_stats_calculator;
    SampleBuffer<std::uint16_t>   &_aggregator;
    current_data::SpiReader       &_reader;
    message_publisher_t            _publisher;
    DataMessage                    _data_message;
    std::unique_ptr<DataCollector> _collector;
    BinarySemaphore                _semaphore;
    QueueHandle_t                  _publisher_queue;
    std::array<char, 2>            _publish_header;
};
