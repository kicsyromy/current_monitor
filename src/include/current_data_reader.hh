#pragma once

#include "sample_buffer.hh"

#include <esp_timer.h>

#include <chrono>
#include <memory>

namespace current_data
{
    class ReaderBase
    {
    public:
        static constexpr std::chrono::microseconds DEFAULT_INTERVAL{ 100 };

    public:
        ReaderBase(SampleBuffer<std::uint16_t> &aggregator);
        virtual ~ReaderBase() = default;

    public:
        void start_reading();
        void set_interval(std::chrono::microseconds interval);
        void make_new_data();

    protected:
        virtual bool read_data(std::uint16_t &) = 0;
        virtual void setup_devices()
        {}

    protected:
        void aggregate_data(std::uint16_t data);

    private:
        SampleBuffer<std::uint16_t> &_aggregator;
        esp_timer_handle_t           _timer;
        std::chrono::microseconds    _interval{ DEFAULT_INTERVAL };
    };

#ifndef CONFIG_CM_SIMULATE_CURRENT_READINGS
    class SpiReader final : public ReaderBase
    {
        struct Device;

    public:
        SpiReader(SampleBuffer<std::uint16_t> &aggregator);
        ~SpiReader() override;

    private:
        bool read_data(std::uint16_t &) override;
        void setup_devices() override;

    private:
        std::unique_ptr<Device> _reader_device;
    };

    SpiReader make_reader(SampleBuffer<std::uint16_t> &aggregator);
#else
    class StaticReader final : public ReaderBase
    {
    public:
        StaticReader(Aggregator &aggregator);
        ~StaticReader() override = default;

    private:
        bool read_data(std::uint16_t &) override;

    private:
        std::size_t _index{ 0 };
    };

    StaticReader make_reader(Aggregator &aggregator);
#endif

} // namespace current_data
