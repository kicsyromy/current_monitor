#pragma once

#include <array>
#include <string>
#include <type_traits>

class DataMessage
{
public:
    enum Reset : std::uint8_t
    {
        None,
    };

    enum Status : std::uint8_t
    {
        Ok,
    };

public:
    DataMessage();

public:
    void set_reset(Reset reset);
    void set_status(Status status);
    void reset();

    void append(std::uint8_t value)
    {
        _message.push_back(static_cast<char>(value));
    }

    void append(std::uint16_t value)
    {
        const auto msb{ static_cast<std::uint8_t>((value >> 8) & 0xFF) };
        const auto lsb{ static_cast<std::uint8_t>(value & 0xFF) };

        append(msb);
        append(lsb);
    }

public:
    std::string &message()
    {
        return _message;
    }
    std::string_view serialize(const std::array<char, 2> &header);
    std::size_t      size() const
    {
        return _message.size();
    }

private:
    Reset       _reset{ Reset::None };
    Status      _status{ Status::Ok };
    std::string _message;
};
