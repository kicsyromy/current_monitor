#pragma once

#include "common.hh"

#ifdef __xtensa__
#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>
#endif

template<std::size_t ResourceCount> struct Semaphore
{
    inline Semaphore() noexcept
    {
        static_assert(ResourceCount >= 2);

#ifdef __xtensa__
        if constexpr (ResourceCount == 2)
        {
            vSemaphoreCreateBinary(handle_);
        }
        else
        {
            handle_ = xSemaphoreCreateCounting(ResourceCount, ResourceCount);
        }
#endif
    }

    inline ~Semaphore() noexcept
    {
#ifdef __xtensa__
        vSemaphoreDelete(handle_);
#endif
    }

    bool take([[maybe_unused]] std::size_t wait_ticks = 100) noexcept
    {
#ifdef __xtensa__
        return xSemaphoreTake(handle_, TickType_t{ wait_ticks }) == pdTRUE;
#else
        if (resource_count_ > 1)
        {
            --resource_count_;
            return true;
        }

        return false;
#endif
    }
    void release() noexcept
    {
#ifdef __xtensa__
        xSemaphoreGive(handle_);
#else
        ++resource_count_;
#endif
    }

#ifdef __xtensa__
    SemaphoreHandle_t handle_{};
#else
    int resource_count_{ ResourceCount };
#endif

private:
    CM_DISABLE_COPY(Semaphore);
    CM_DISABLE_MOVE(Semaphore);
};

using BinarySemaphore = Semaphore<2>;
