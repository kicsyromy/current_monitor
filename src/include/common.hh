#pragma once

#include <array>
#include <cstdint>
#include <string_view>

#ifdef CONFIG_CM_WIFI_SSID
constexpr std::string_view WIFI_SSID{ CONFIG_CM_WIFI_SSID };
#else
constexpr std::string_view WIFI_SSID{ "" };
#endif

#ifdef CONFIG_CM_WIFI_PASSWORD
constexpr std::string_view WIFI_PASSWORD{ CONFIG_CM_WIFI_PASSWORD };
#else
constexpr std::string_view WIFI_PASSWORD{ "" };
#endif

#ifdef CONFIG_CM_WIFI_MAXIMUM_RETRIES
constexpr std::uint8_t WIFI_MAXIMUM_RETRIES{ CONFIG_CM_WIFI_MAXIMUM_RETRIES };
#else
constexpr std::uint8_t WIFI_MAXIMUM_RETRIES{ 5 };
#endif

#ifdef CONFIG_CM_MQTT_BROKER_URL
constexpr std::string_view MQTT_BROKER_URL{ CONFIG_CM_MQTT_BROKER_URL };
#else
constexpr std::string_view MQTT_BROKER_URL{
    "mqtt://f96c1ea9d3924042871342ee37420662.s1.eu.hivemq.cloud"
};
#endif

#ifdef CONFIG_CM_MQTT_TLS_PORT
constexpr auto MQTT_TLS_PORT = std::uint32_t{ CONFIG_CM_MQTT_TLS_PORT };
#else
constexpr auto MQTT_TLS_PORT = std::uint32_t{ 8883 };
#endif

#ifdef CONFIG_CM_MQTT_USERNAME
constexpr std::string_view MQTT_USERNAME{ CONFIG_CM_MQTT_USERNAME };
#else
constexpr std::string_view MQTT_USERNAME{ "mqttest" };
#endif

#ifdef CONFIG_CM_MQTT_PASSWORD
constexpr std::string_view MQTT_PASSWORD{ CONFIG_CM_MQTT_PASSWORD };
#else
constexpr std::string_view MQTT_PASSWORD{ "Zomfg1234" };
#endif

#define CM_DISABLE_COPY(klass)              \
    explicit klass(const klass &) = delete; \
    klass &operator=(const klass &) = delete

#define CM_DISABLE_MOVE(klass)         \
    explicit klass(klass &&) = delete; \
    klass &operator=(klass &&) = delete

#define CM_UNUSED(var) static_cast<void>(var)

constexpr auto APP_TAG{ "CurrentMonitor" };

namespace utility
{
    template<std::size_t S>
    constexpr std::size_t get_file_name_offset(const char (&path)[S], size_t i = S - 1) noexcept
    {
        return (path[i] == '/' || path[i] == '\\')
                   ? i + 1
                   : (i > 0 ? get_file_name_offset(path, i - 1) : 0);
    }

    template<typename...> struct GetClassType : std::false_type
    {
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...)>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) const>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) volatile>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) const volatile>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) const noexcept>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) noexcept>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) volatile noexcept>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) const volatile noexcept>
    {
        using Type = Class;
    };
} // namespace utility