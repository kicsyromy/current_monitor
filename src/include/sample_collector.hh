#pragma once
#include "stats_collector.hh"
#include <chrono>
#include <data_collector.hh>
#include <data_publisher.hh>

class SampleCollector : public DataCollector
{
public:
    SampleCollector(DataPublisher &data_publisher,
                    DataMessage   &data_message,
                    std::size_t    data_count)
      : DataCollector(data_publisher, data_message, data_count)
    {
        CM_LOG_INFO_F("Creating new sample data collector data_count {}", data_count);
    }

    ~SampleCollector() override = default;

public:
    void collect(const std::vector<std::uint16_t> &snapshot) override
    {
        collect_stats(snapshot);
        collect_sample_data(snapshot);
        _filled = true;
    }

    bool is_filled() const override
    {
        return _filled;
    }

    void message_published() override
    {
        _filled = false;
    }

    std::string_view data_topic() const override
    {
        return CONFIG_CM_SAMPLES_PUBLISH_TOPIC;
    }

private:
    void collect_stats(const std::vector<std::uint16_t> &snapshot)
    {
        const auto stats = _publisher.stats_calculator().calculate(snapshot);
        const auto data  = StatsCollector::StatsData{ stats };
        data.serialize(_data_message);
    }

    void collect_sample_data(const std::vector<std::uint16_t> &snapshot)
    {
        CM_LOG_INFO_F("Buffer size: {}", snapshot.size());

        std::size_t step{ 1 };
        if (_data_count < snapshot.size())
        {
            step = snapshot.size() / _data_count;
        }

        for (std::size_t i = 0; i < snapshot.size(); i += step)
        {
            const auto value = snapshot[i];
            const auto msb   = static_cast<std::uint8_t>((value >> 8) & 0xff);
            const auto lsb   = static_cast<std::uint8_t>(value & 0xFF);

            _data_message.message().push_back(static_cast<char>(msb));
            _data_message.message().push_back(static_cast<char>(lsb));
        }
    }

private:
    bool _filled{ false };
};
