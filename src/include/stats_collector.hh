#pragma once

#include "data_collector.hh"
#include "data_publisher.hh"
#include "statistics.hh"

#include <cstdint>

class StatsCollector : public DataCollector
{
public:
    struct StatsData
    {
        StatsData(const statistics::Statistics &stats)
          : pulse_count(stats.pulse_count())
          , pulse_amplitude_average(stats.pulse_amplitude_average())
          , pulse_width_average(stats.pulse_width_average())
        {}

        void serialize(DataMessage &buffer) const
        {
            buffer.append(pulse_count);
            buffer.append(pulse_amplitude_average);
            buffer.append(pulse_width_average);
        }

        uint8_t  pulse_count;
        uint16_t pulse_amplitude_average;
        uint8_t  pulse_width_average;
    };

public:
    StatsCollector(DataPublisher &data_publisher, DataMessage &data_message, std::size_t data_count)
      : DataCollector(data_publisher, data_message, data_count)
    {
        CM_LOG_INFO_F("Creating new stats data collector data_count {}", data_count);
    }

public:
    void collect(const std::vector<std::uint16_t> &snapshot) override
    {
        const auto stats          = _publisher.stats_calculator().calculate(snapshot);
        auto       stats_instance = StatsData{ stats };
        CM_LOG_INFO_F("--------- {} {} {}",
                      stats_instance.pulse_count,
                      stats_instance.pulse_amplitude_average,
                      stats_instance.pulse_width_average);
        _stats_list.push_back(std::move(stats_instance));
        if (is_filled())
        {
            serialize_stats();
        }
    }

    bool is_filled() const override
    {
        return _stats_list.size() >= _data_count;
    }

    void message_published() override
    {
        _stats_list.clear();
    }

    std::string_view data_topic() const override
    {
        return CONFIG_CM_STATS_PUBLISH_TOPIC;
    }

private:
    void serialize_stats()
    {
        for (const auto &el : _stats_list)
        {
            _data_message.append(el.pulse_count);
        }

        for (const auto &el : _stats_list)
        {
            _data_message.append(el.pulse_amplitude_average);
        }

        for (const auto &el : _stats_list)
        {
            _data_message.append(el.pulse_width_average);
        }
    }

private:
    std::vector<StatsData> _stats_list;
};
