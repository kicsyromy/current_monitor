#pragma once

#include "common.hh"

#include <fmt/format.h>

#ifdef __xtensa__
#include <esp_log.h>
#else
#include <cstdio>
#endif

#define CM_LOG_TO_STRING_IMP(x) #x
#define CM_LOG_TO_STRING(x) CM_LOG_TO_STRING_IMP(x)

#define CM_LOG_TRACE_F(format, ...)                                                           \
    do                                                                                        \
    {                                                                                         \
        constexpr auto format_specifier =                                                     \
            Logger::construct_format_specifier(__FILE__, CM_LOG_TO_STRING(__LINE__), format); \
        Logger::trace(std::string_view{ format_specifier.data() }, __VA_ARGS__);              \
    } while (false)
#define CM_LOG_TRACE(message) CM_LOG_TRACE_F("{}", message)

#define CM_LOG_DEBUG_F(format, ...)                                                           \
    do                                                                                        \
    {                                                                                         \
        constexpr auto format_specifier =                                                     \
            Logger::construct_format_specifier(__FILE__, CM_LOG_TO_STRING(__LINE__), format); \
        Logger::debug(std::string_view{ format_specifier.data() }, __VA_ARGS__);              \
    } while (false)
#define CM_LOG_DEBUG(message) CM_LOG_DEBUG_F("{}", message)

#define CM_LOG_INFO_F(format, ...)                                                            \
    do                                                                                        \
    {                                                                                         \
        constexpr auto format_specifier =                                                     \
            Logger::construct_format_specifier(__FILE__, CM_LOG_TO_STRING(__LINE__), format); \
        Logger::info(std::string_view{ format_specifier.data() }, __VA_ARGS__);               \
    } while (false)
#define CM_LOG_INFO(message) CM_LOG_INFO_F("{}", message)

#define CM_LOG_WARN_F(format, ...)                                                            \
    do                                                                                        \
    {                                                                                         \
        constexpr auto format_specifier =                                                     \
            Logger::construct_format_specifier(__FILE__, CM_LOG_TO_STRING(__LINE__), format); \
        Logger::warn(std::string_view{ format_specifier.data() }, __VA_ARGS__);               \
    } while (false)
#define CM_LOG_WARN(message) CM_LOG_WARN_F("{}", message)

#define CM_LOG_ERROR_F(format, ...)                                                           \
    do                                                                                        \
    {                                                                                         \
        constexpr auto format_specifier =                                                     \
            Logger::construct_format_specifier(__FILE__, CM_LOG_TO_STRING(__LINE__), format); \
        Logger::error(std::string_view{ format_specifier.data() }, __VA_ARGS__);              \
    } while (false)
#define CM_LOG_ERROR(message) CM_LOG_ERROR_F("{}", message)

struct Logger
{
    static constexpr std::size_t LOG_BUFFER_SIZE{ 255 };

    template<typename... Args> static constexpr void trace(std::string_view format, Args &&...args)
    {
        auto buffer = format_string(format, std::forward<Args>(args)...);
#ifdef __xtensa__
        ESP_LOGV(APP_TAG, "%s", buffer.data());
#else
        std::printf("TRACE: %s\n", buffer.data());
#endif
    }

    template<typename... Args> static constexpr void debug(std::string_view format, Args &&...args)
    {
        auto buffer = format_string(format, std::forward<Args>(args)...);
#ifdef __xtensa__
        ESP_LOGD(APP_TAG, "%s", buffer.data());
#else
        std::printf("DEBUG: %s\n", buffer.data());
#endif
    }

    template<typename... Args> static constexpr void info(std::string_view format, Args &&...args)
    {
        auto buffer = format_string(format, std::forward<Args>(args)...);
#ifdef __xtensa__
        ESP_LOGI(APP_TAG, "%s", buffer.data());
#else
        std::printf("INFO: %s\n", buffer.data());
#endif
    }

    template<typename... Args> static constexpr void warn(std::string_view format, Args &&...args)
    {
        auto buffer = format_string(format, std::forward<Args>(args)...);
#ifdef __xtensa__
        ESP_LOGW(APP_TAG, "%s", buffer.data());
#else
        std::fprintf(stderr, "WARN: %s\n", buffer.data());
#endif
    }

    template<typename... Args> static constexpr void error(std::string_view format, Args &&...args)
    {
        auto buffer = format_string(format, std::forward<Args>(args)...);
#ifdef __xtensa__
        ESP_LOGE(APP_TAG, "%s", buffer.data());
#else
        std::fprintf(stderr, "ERROR: %s\n", buffer.data());
#endif
    }

    template<std::size_t FilePathSize, std::size_t LineNumberSize, std::size_t FormatSpecifierSize>
    static constexpr auto construct_format_specifier(const char (&file_path)[FilePathSize],
        const char (&line)[LineNumberSize],
        const char (&format_specifier)[FormatSpecifierSize]) noexcept
    {
        const auto file_name_offset = utility::get_file_name_offset(file_path);
        std::array<char, FilePathSize + LineNumberSize + FormatSpecifierSize + 2 + 1> result{};

        std::size_t position = 0;
        for (std::size_t i = 0; i < FilePathSize - file_name_offset - 1; ++i, ++position)
        {
            result[position] = file_path[i + file_name_offset];
        }
        result[position++] = ':';
        for (std::size_t i = 0; i < LineNumberSize - 1; ++i, ++position)
        {
            result[position] = line[i];
        }
        result[position++] = ':';
        result[position++] = ' ';

        for (std::size_t i = 0; i < FormatSpecifierSize - 1; ++i, ++position)
        {
            result[position] = format_specifier[i];
        }

        result[position] = '\0';

        return result;
    }

private:
    template<typename... Args>
    static constexpr auto format_string(std::string_view format, Args &&...args)
    {
        std::array<char, LOG_BUFFER_SIZE + 1> log_buffer{ 0 };

        fmt::format_to_n(log_buffer.data(), LOG_BUFFER_SIZE, format, std::forward<Args>(args)...);
        return log_buffer;
    }
};