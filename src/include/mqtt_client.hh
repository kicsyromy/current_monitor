#pragma once

#include <string>
#include <string_view>

#include "common.hh"
#include "signal.hh"

#ifdef __xtensa__
#include <mqtt_client.h>

using MQTTClientHandle = esp_mqtt_client_handle_t;
using EventBase = esp_event_base_t;
#else
using MQTTClientHandle = void *;
using EventBase = void *;
#endif
struct MQTTClient
{
    using MessageID = std::int32_t;

    enum struct Error
    {
        TCPTransportError = 0,
        BadProtocol,
        IDRejected,
        ServerUnavailable,
        BadUsername,
        NotAuthorized,
        Unknown = 255
    };

    void set_broker_url(std::string_view url, std::uint32_t port) noexcept;
    void set_username(std::string_view username) noexcept;
    void set_password(std::string_view password) noexcept;

    void connect() noexcept;
    void disconnect() noexcept;
    MessageID publish_message(std::string_view topic, std::string_view message) noexcept;
    MessageID subscribe(std::string_view topic);

signals:
    Signal<void(MQTTClient *self)> connected;
    Signal<void(MQTTClient *self, std::string_view)> subscribed;
    Signal<void(MQTTClient *self, std::string_view)> unsubscribed;
    Signal<void(MQTTClient *self, MessageID)> published;
    Signal<void(MQTTClient *self, std::string_view topic, const std::string &data)> received;
    Signal<void(MQTTClient *self, Error)> error;
    Signal<void(MQTTClient *self)> disconnected;

private:
    static void event_handler(void *instance,
        EventBase base,
        std::int32_t event_id,
        void *event_data);

private:
    MQTTClientHandle client_handle_{};
    std::string url_{};
    std::string username_{};
    std::string password_{};
    std::uint32_t port_{};
};