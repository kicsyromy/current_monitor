#pragma once
#ifdef __xtensa__
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#else
#include <thread>

#define portTICK_PERIOD_MS     1
#define xTaskGetTickCount(...) 0

static constexpr auto esp_timer_get_time = [](auto...) {
    return 0;
};

using TickType_t = std::size_t;
#endif
#include <chrono>

#define PACKED __attribute__((packed))

namespace utils
{
    template<typename Duration> static constexpr TickType_t duration_to_ticks(Duration duration)
    {
        using tick_duration = std::chrono::milliseconds;
        tick_duration ms_duration{ duration.count() };
        if constexpr (std::is_same<Duration, tick_duration>::value == false)
        {
            ms_duration = std::chrono::duration_cast<tick_duration>(duration);
        }

        return static_cast<std::size_t>(ms_duration.count()) / portTICK_PERIOD_MS;
    }

    template<typename Duration> static constexpr Duration ticks_to_duration(TickType_t ticks)
    {
        using tick_duration = std::chrono::milliseconds;

        std::chrono::milliseconds ms_duration(ticks * portTICK_PERIOD_MS);
        if constexpr (std::is_same<Duration, tick_duration>::value)
        {
            return ms_duration;
        }
        else
        {
            return std::chrono::duration_cast<Duration>(ms_duration);
        }
    }

    template<typename Duration> void delay(Duration duration)
    {
#ifdef __xtensa__
        vTaskDelay(duration_to_ticks(duration));
#else
        std::this_thread::sleep_for(duration);
#endif
    }

    static inline void delay_microseconds(std::uint32_t us)
    {
        const auto m = static_cast<std::uint64_t>(esp_timer_get_time());
        if (us)
        {
            uint64_t e = (m + us);
            if (m > e)
            { // overflow
                while (static_cast<std::uint64_t>(esp_timer_get_time()) > e)
                {
                    __asm volatile("nop");
                }
            }
            while (static_cast<std::uint64_t>(esp_timer_get_time()) < e)
            {
                __asm volatile("nop");
            }
        }
    }

    template<typename T> void clear(T &obj)
    {
        memset(&obj, 0, sizeof(T));
    }

    static inline TickType_t get_timestamp()
    {
        return xTaskGetTickCount();
    }
} // namespace utils
