#pragma once

#include <nano_mutex.hpp>
#include <nano_signal_slot.hpp>

#define signals public

template<typename Signature> using Signal = Nano::Signal<Signature, Nano::ST_Policy_Safe>;
