#pragma once

#if __xtensa__
#include <nvs_flash.h>
#else
#define ESP_OK               0
#define ESP_ERROR_CHECK(arg) static_cast<void>(arg)

#define NVS_READWRITE 0

static constexpr auto nvs_open = [](auto...) {
    return 0;
};
static constexpr auto nvs_close = [](auto...) {
    return 0;
};
static constexpr auto nvs_commit = [](auto...) {
    return 0;
};
static constexpr auto nvs_set_i8 = [](auto...) {
    return 0;
};
static constexpr auto nvs_set_u8 = [](auto...) {
    return 0;
};
static constexpr auto nvs_set_i16 = [](auto...) {
    return 0;
};
static constexpr auto nvs_set_u16 = [](auto...) {
    return 0;
};
static constexpr auto nvs_set_i32 = [](auto...) {
    return 0;
};
static constexpr auto nvs_set_u32 = [](auto...) {
    return 0;
};
static constexpr auto nvs_set_i64 = [](auto...) {
    return 0;
};
static constexpr auto nvs_set_u64 = [](auto...) {
    return 0;
};
static constexpr auto nvs_set_str = [](auto...) {
    return 0;
};
static constexpr auto nvs_get_i8 = [](auto...) {
    return 0;
};
static constexpr auto nvs_get_u8 = [](auto...) {
    return 0;
};
static constexpr auto nvs_get_i16 = [](auto...) {
    return 0;
};
static constexpr auto nvs_get_u16 = [](auto...) {
    return 0;
};
static constexpr auto nvs_get_i32 = [](auto...) {
    return 0;
};
static constexpr auto nvs_get_u32 = [](auto...) {
    return 0;
};
static constexpr auto nvs_get_i64 = [](auto...) {
    return 0;
};
static constexpr auto nvs_get_u64 = [](auto...) {
    return 0;
};
static constexpr auto nvs_get_str = [](auto...) {
    return 0;
};

using esp_err_t    = int;
using nvs_handle_t = int;
#endif

#include <cstdint>
#include <string>
#include <string_view>

namespace config::nvs
{
    class Proxy
    {
    public:
        Proxy(std::string_view namespace_)
        {
            const auto err = nvs_open(namespace_.data(), NVS_READWRITE, &_handle);
            ESP_ERROR_CHECK(err);
        }

        ~Proxy()
        {
            const auto err = nvs_commit(_handle);
            ESP_ERROR_CHECK(err);
            nvs_close(_handle);
        }

    public:
        esp_err_t save(std::string_view key, std::int8_t value)
        {
            return nvs_set_i8(_handle, key.data(), value);
        }

        esp_err_t save(std::string_view key, std::uint8_t value)
        {
            return nvs_set_u8(_handle, key.data(), value);
        }

        esp_err_t save(std::string_view key, std::int16_t value)
        {
            return nvs_set_i16(_handle, key.data(), value);
        }

        esp_err_t save(std::string_view key, std::uint16_t value)
        {
            return nvs_set_u16(_handle, key.data(), value);
        }

        esp_err_t save(std::string_view key, std::int32_t value)
        {
            return nvs_set_i32(_handle, key.data(), value);
        }

        esp_err_t save(std::string_view key, std::uint32_t value)
        {
            return nvs_set_u32(_handle, key.data(), value);
        }

        esp_err_t save(std::string_view key, std::int64_t value)
        {
            return nvs_set_i64(_handle, key.data(), value);
        }

        esp_err_t save(std::string_view key, std::uint64_t value)
        {
            return nvs_set_u64(_handle, key.data(), value);
        }

        esp_err_t save(std::string_view key, std::string_view value)
        {
            return nvs_set_str(_handle, key.data(), value.data());
        }

    public:
        esp_err_t load(std::string_view key, std::int8_t &value)
        {
            return nvs_get_i8(_handle, key.data(), &value);
        }

        esp_err_t load(std::string_view key, std::uint8_t &value)
        {
            return nvs_get_u8(_handle, key.data(), &value);
        }

        esp_err_t load(std::string_view key, std::int16_t &value)
        {
            return nvs_get_i16(_handle, key.data(), &value);
        }

        esp_err_t load(std::string_view key, std::uint16_t &value)
        {
            return nvs_get_u16(_handle, key.data(), &value);
        }

        esp_err_t load(std::string_view key, std::int32_t &value)
        {
            return nvs_get_i32(_handle, key.data(), &value);
        }

        esp_err_t load(std::string_view key, std::uint32_t &value)
        {
            return nvs_get_u32(_handle, key.data(), &value);
        }

        esp_err_t load(std::string_view key, std::int64_t &value)
        {
            return nvs_get_i64(_handle, key.data(), &value);
        }

        esp_err_t load(std::string_view key, std::uint64_t &value)
        {
            return nvs_get_u64(_handle, key.data(), &value);
        }

        esp_err_t load(std::string_view key, std::string &value)
        {
            std::size_t str_size;
            auto        err = nvs_get_str(_handle, key.data(), nullptr, &str_size);
            if (err != ESP_OK)
            {
                return err;
            }

            std::string out(str_size, '_');
            err   = nvs_get_str(_handle, key.data(), out.data(), &str_size);
            value = out;

            return err;
        }

        esp_err_t commit()
        {
            return nvs_commit(_handle);
        }

    private:
        nvs_handle_t _handle;
    };
} // namespace config::nvs