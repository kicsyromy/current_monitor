#pragma once
#include <semaphore.hh>
#include <logger.hh>

namespace config
{

    template<typename Config>
    class Configurable
    {
    public:
        Configurable() = default;
        virtual ~Configurable() = default;

    public:
        Config get_config()
        {
            Config cfg {};
            if (_sem.take())
            {
                cfg = _config;
                _sem.release();
            }
            else
            {
                CM_LOG_ERROR("Failed to take semaphore, returning defaults");
            }

            return cfg;
        }

        void set_config(const Config& config)
        {
            if (_sem.take())
            {
                _config = config;
                config_changed(_config);
                _sem.release();
            }
            else
            {
                CM_LOG_ERROR("Failed to take semaphore, not changing value");
            }
        }

    protected:
        virtual void config_changed(const Config&) = 0;

    private:
        BinarySemaphore _sem;
        Config _config { Config {}};
    };
}
