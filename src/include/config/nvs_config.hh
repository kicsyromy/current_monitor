#pragma once

namespace config
{
    namespace nvs
    {
        class Proxy;

        class Config
        {
        public:
            Config() = default;
            virtual ~Config() = default;

        public:
            Config(const Config&) = default;
            Config& operator=(const Config&) = default;

        public:
            virtual bool load(nvs::Proxy&) = 0;
            virtual bool save(nvs::Proxy&) = 0;
        };
    }
}
