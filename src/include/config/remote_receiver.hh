#pragma once
#include <functional>
#include <logger.hh>
#include <string_view>
#include <unordered_map>
#include <utils.hh>

namespace config
{
    class RemoteReceiver
    {
        static constexpr uint32_t HEADER_MAGIC{ 0xDEADBEEF };
        struct PACKED Header
        {
            std::uint32_t magic;
            std::uint8_t size;
            std::uint8_t message_type;
        };

    public:
        enum MessageType : std::uint8_t
        {
            DataPublisher,
            StatisticsCalculator,
            FirmwareUpdate,
        };

        using new_config_decoder_t = std::function<void(std::string_view)>;
        enum Result : std::uint8_t
        {
            Ok = 0x01,
            InvalidHeaderSize,
            InvalidMagic,
            InvalidContentSize,
            InvalidType
        };

    public:
        std::string message_received(std::string_view message)
        {
            static_assert(sizeof(Result) == sizeof(char), "Invalid size of Result enum");

            const auto result{ handle_message(message) };
            return std::string{ sizeof(result), static_cast<char>(result) };
        }

    public:
        bool register_decoder(MessageType message_type,
            new_config_decoder_t &&decoder,
            bool overwrite = false)
        {
            auto it = _decoders.find(message_type);
            if (it != _decoders.end())
            {
                if (overwrite)
                {
                    it->second = std::move(decoder);
                    return true;
                }
                return false;
            }
            else
            {
                _decoders[message_type] = std::move(decoder);
                return true;
            }
            return false;
        }

        bool unregister_decoder(MessageType message_type)
        {
            auto it = _decoders.find(message_type);
            if (it != _decoders.end())
            {
                _decoders.erase(it);
                return true;
            }
            return false;
        }

    private:
        Result handle_message(std::string_view message)
        {
            if (message.size() < sizeof(Header))
            {
                CM_LOG_ERROR_F("Invalid header size actual: {} expected: {}",
                    message.size(),
                    sizeof(Header));
                return Result::InvalidHeaderSize;
            }

            auto header = reinterpret_cast<const Header *>(message.data());
            if (header->magic != HEADER_MAGIC)
            {
                CM_LOG_ERROR_F("Invalid magic actual: {} expected: {}",
                    header->magic,
                    HEADER_MAGIC);
                return Result::InvalidMagic;
            }

            if (message.size() < (sizeof(Header) + header->size))
            {
                CM_LOG_ERROR_F("Invalid content size actual: {}, expected: {}",
                    message.size(),
                    (sizeof(Header) + header->size));
                return Result::InvalidContentSize;
            }

            const auto message_type = static_cast<MessageType>(header->message_type);
            message.remove_prefix(sizeof(Header));
            return dispatch(message_type, message);
        }

        Result dispatch(MessageType message_type, std::string_view message_data)
        {
            auto it = _decoders.find(message_type);
            if (it != _decoders.end())
            {
                it->second(message_data);
                return Result::Ok;
            }

            return Result::InvalidType;
        }

    private:
        std::unordered_map<MessageType, new_config_decoder_t> _decoders;
    };

} // namespace config