#pragma once

#include "logger.hh"
#include "signal.hh"

#include <cstring>
#include <utility>
#include <vector>

template<typename T> struct SampleBuffer
{
    using BufferType = std::vector<T>;
    using DataType   = T;

    [[nodiscard]] inline bool push_one(const T *data) noexcept
    {
        if (!data || cursor_ + 1 > active_buffer_->size())
        {
            return false;
        }

        std::memcpy(&(active_buffer_->data()[cursor_]), data, sizeof(T));
        // printf("+++++++++++++++++ %u %u\n", *data, (*active_buffer_)[cursor_]);
        ++cursor_;

        if (cursor_ == active_buffer_->size())
        {
            swap_buffers();
            buffer_full.fire(this);
        }

        return true;
    }

    [[nodiscard]] inline bool push_many(const T *data, std::size_t count) noexcept
    {
        if (!data || cursor_ + count > active_buffer_->size())
        {
            return false;
        }

        std::memcpy(&(active_buffer_->data()[cursor_]), data, count * sizeof(T));
        cursor_ += count;

        if (cursor_ == active_buffer_->size())
        {
            swap_buffers();
            buffer_full.fire(this);
        }

        return true;
    }

    void swap_buffers() noexcept
    {
        std::swap(active_buffer_, inactive_buffer_);
        cursor_ = 0;
    }

    [[nodiscard]] auto &active_buffer() const noexcept
    {
        return *active_buffer_;
    }

    [[nodiscard]] auto &inactive_buffer() const noexcept
    {
        return *inactive_buffer_;
    }

    void resize(std::size_t new_size)
    {
        CM_LOG_INFO_F("Resizing buffer to {} elements", new_size);

        cursor_ = 0;
        buffer1_.resize(new_size);
        buffer2_.resize(new_size);
    }

signals:
    Signal<void(SampleBuffer<T> *)> buffer_full;

private:
    BufferType buffer1_;
    BufferType buffer2_;

    BufferType *active_buffer_{ &buffer1_ };
    BufferType *inactive_buffer_{ &buffer2_ };
    std::size_t cursor_{ 0 };
};