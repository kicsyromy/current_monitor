#pragma once

#include "data_message.hh"
#include "logger.hh"
#include "sample_buffer.hh"

#include <cstdint>
#include <memory>

class DataPublisher;

class DataCollector
{
public:
    enum CollectorType : std::uint8_t
    {
        Stats,
        Sample,
        COLLECTOR_TYPE_COUNT
    };

public:
    DataCollector(DataPublisher &, DataMessage &data_message, std::size_t data_count);
    virtual ~DataCollector() = default;

public:
    virtual bool             is_filled() const                           = 0;
    virtual void             collect(const std::vector<std::uint16_t> &) = 0;
    virtual void             message_published()                         = 0;
    virtual std::string_view data_topic() const                          = 0;

public:
    static std::unique_ptr<DataCollector> make_new(DataPublisher &,
                                                   CollectorType,
                                                   DataMessage &,
                                                   std::size_t);

protected:
    DataPublisher &_publisher;
    DataMessage   &_data_message;
    std::size_t    _data_count;
};
