#pragma once

#include "config/configurable.hh"
#include "config/nvs_config.hh"
#include "config/remote_receiver.hh"
#include "logger.hh"
#include "sample_buffer.hh"
#include "utils.hh"

#include <cmath>
#include <cstdint>
#include <limits>
#include <vector>

#define ADC_VALUE_MULTIPLIER 100
#define ADC_VALUE_ZERO       13200
#define ADC_VALUE_STEP       0.01136370882

namespace statistics
{
    struct Statistics
    {
        struct Config final : public config::nvs::Config
        {
            static constexpr std::uint16_t DEFAULT_PULSE_THRESHOLD{ 49150 };  // 50 Amps
            static constexpr std::uint8_t  DEFAULT_PULSE_LENGTH{ 8 };

        public:
            struct PACKED Data
            {
                std::uint16_t pulse_threshold;
                std::uint8_t  pulse_length;
            };

        public:
            Config() = default;
            Config(const Data &data)
              : pulse_threshold(data.pulse_threshold)
              , pulse_length(data.pulse_length)
            {}

        public:
            bool load(config::nvs::Proxy &);
            bool save(config::nvs::Proxy &);

        public:
            std::uint16_t pulse_threshold{ DEFAULT_PULSE_THRESHOLD };
            std::uint8_t  pulse_length{ DEFAULT_PULSE_LENGTH };
        };

        constexpr auto pulse_count() const noexcept
        {
            return pulse_count_;
        }

        constexpr auto pulse_amplitude_average() const noexcept
        {
            return pulse_amplitude_average_;
        }

        constexpr auto pulse_width_average() const noexcept
        {
            return pulse_width_average_;
        }

        static Statistics compute_raw(const std::vector<std::uint16_t> &snapshot,
                                      const Config                     &cfg) noexcept;

    private:
        std::uint8_t  pulse_count_{ 0 };
        std::uint16_t pulse_amplitude_average_{ 0 };
        std::uint8_t  pulse_width_average_{ 0 };
    };

    class Calculator final : public config::Configurable<Statistics::Config>
    {
    public:
        Calculator(config::nvs::Proxy &nvs_proxy, config::RemoteReceiver &remote_config_receiver);
        ~Calculator();

    public:
        Statistics calculate(const std::vector<std::uint16_t> &snapshot)
        {
            const auto config = get_config();
            return Statistics::compute_raw(snapshot, config);
        }

    private:
        void config_changed(const Statistics::Config &) override;

    private:
        config::nvs::Proxy     &_nvs_proxy;
        config::RemoteReceiver &_remote_cfg_receiver;
    };
} // namespace statistics
