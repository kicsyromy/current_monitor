#pragma once

extern "C" const uint8_t CERT_ISRG_X1_START[] asm("_binary_ISRG_Root_X1_pem_start");
extern "C" const uint8_t CERT_ISRG_X1_END[] asm("_binary_ISRG_Root_X1_pem_end");

extern "C" const uint8_t OTA_SERVER_CERT_START[] asm("_binary_ota_server_cert_pem_start");
extern "C" const uint8_t OTA_SERVER_CERT_END[] asm("_binary_ota_server_cert_pem_end");
