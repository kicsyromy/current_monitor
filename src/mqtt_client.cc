#include <array>

#include "common.hh"
#include "logger.hh"
#include "mqtt_client.hh"

#ifdef __xtensa__

#include "esp_tls.h"
#include "mqtt_client.h"

void MQTTClient::connect() noexcept
{
    auto mqtt_config = esp_mqtt_client_config_t{};
    mqtt_config.client_id = CONFIG_CM_MQTT_CLIENT_ID;
    mqtt_config.uri = url_.c_str();
    mqtt_config.username = username_.c_str();
    mqtt_config.password = password_.c_str();
    mqtt_config.port = port_;
    mqtt_config.use_global_ca_store = true;

    client_handle_ = esp_mqtt_client_init(&mqtt_config);
    esp_mqtt_client_register_event(client_handle_,
        static_cast<esp_mqtt_event_id_t>(ESP_EVENT_ANY_ID),
        &MQTTClient::event_handler,
        this);
    const auto esp_err = esp_mqtt_client_start(client_handle_);
    ESP_ERROR_CHECK(esp_err);
}

void MQTTClient::disconnect() noexcept
{
    esp_mqtt_client_disconnect(client_handle_);
    esp_mqtt_client_destroy(client_handle_);
}

MQTTClient::MessageID MQTTClient::publish_message(std::string_view topic,
    std::string_view message) noexcept
{
    return esp_mqtt_client_publish(client_handle_,
        topic.data(),
        message.data(),
        static_cast<int>(message.size()),
        0,
        0);
}

MQTTClient::MessageID MQTTClient::subscribe(std::string_view topic)
{
    return esp_mqtt_client_subscribe(
        client_handle_,
        topic.data(),
        0
    );
}

void MQTTClient::event_handler(void *instance,
    EventBase base,
    std::int32_t event_id,
    void *event_data)
{
    CM_UNUSED(base);

    CM_LOG_INFO_F("MQTT event: {}", event_id);

    static constexpr auto TOPIC_MAX_LEN = std::size_t{ 128 };
    auto *self = static_cast<MQTTClient *>(instance);

    auto topic_buffer = std::array<char, TOPIC_MAX_LEN + 1>{ 0 };
    const auto copy_topic = [&topic_buffer] (char* source, std::size_t size) {
        const auto topic_length = std::min(TOPIC_MAX_LEN, size);
        std::memcpy(topic_buffer.data(), source, topic_length);
        return topic_length;
    };

    esp_mqtt_event_handle_t event = static_cast<esp_mqtt_event_handle_t>(event_data);
    switch (static_cast<esp_mqtt_event_id_t>(event_id))
    {
    case MQTT_EVENT_CONNECTED:
        self->connected.fire(self);
        break;

    case MQTT_EVENT_DISCONNECTED:
        self->disconnected.fire(self);
        break;

    case MQTT_EVENT_SUBSCRIBED: {
        const auto topic_length = copy_topic(event->topic, static_cast<std::size_t>(event->topic_len));
        self->subscribed.fire(self, std::string_view{ topic_buffer.data(), topic_length });

        break;
    }

    case MQTT_EVENT_UNSUBSCRIBED: {
        const auto topic_length = copy_topic(event->topic, static_cast<std::size_t>(event->topic_len));
        self->unsubscribed.fire(self, std::string_view{ topic_buffer.data(), topic_length });

        break;
    }

    case MQTT_EVENT_PUBLISHED:
        self->published.fire(self, event->msg_id);
        break;

    case MQTT_EVENT_DATA: {
        const auto topic_length = copy_topic(event->topic, static_cast<std::size_t>(event->topic_len));

        self->received.fire(self,
            std::string_view{ topic_buffer.data(), topic_length },
            std::string{ event->data, static_cast<std::size_t>(event->data_len) });

        break;
    }

    case MQTT_EVENT_ERROR: {
        static_assert(static_cast<int>(Error::BadProtocol) == MQTT_CONNECTION_REFUSE_PROTOCOL);
        static_assert(static_cast<int>(Error::IDRejected) == MQTT_CONNECTION_REFUSE_ID_REJECTED);
        static_assert(static_cast<int>(Error::ServerUnavailable) ==
                      MQTT_CONNECTION_REFUSE_SERVER_UNAVAILABLE);
        static_assert(static_cast<int>(Error::BadUsername) == MQTT_CONNECTION_REFUSE_BAD_USERNAME);
        static_assert(
            static_cast<int>(Error::NotAuthorized) == MQTT_CONNECTION_REFUSE_NOT_AUTHORIZED);

        if (event->error_handle->error_type == MQTT_ERROR_TYPE_TCP_TRANSPORT)
        {
            self->error.fire(self, Error::TCPTransportError);
        }
        else if (event->error_handle->error_type == MQTT_ERROR_TYPE_CONNECTION_REFUSED)
        {
            self->error.fire(self, static_cast<Error>(event->error_handle->connect_return_code));
        }
        else
        {
            self->error.fire(self, Error::Unknown);
        }

        break;
    }
    default:
        break;
    }
}

#else

void MQTTClient::connect() noexcept
{
    CM_UNUSED(client_handle_);
}

MQTTClient::MessageID MQTTClient::publish_message(std::string_view, std::string_view) noexcept
{
    return {};
}

void MQTTClient::event_handler(void *, EventBase, std::int32_t, void *)
{}

#endif

void MQTTClient::set_broker_url(std::string_view url, std::uint32_t port) noexcept
{
    url_.clear();
    url_.append(url);
    port_ = port;
}

void MQTTClient::set_username(std::string_view username) noexcept
{
    username_.clear();
    username_.append(username);
}

void MQTTClient::set_password(std::string_view password) noexcept
{
    password_.clear();
    password_.append(password);
}
