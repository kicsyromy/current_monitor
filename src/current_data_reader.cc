#include "current_data_reader.hh"
#include "current_reader_constants.hh"
#include "utils.hh"

using namespace current_data;

void timer_callback(void *arg)
{
    ReaderBase *reader = static_cast<ReaderBase *>(arg);
    reader->make_new_data();
}

static esp_timer_handle_t make_timer(void *callback_arg)
{
    esp_timer_create_args_t timer_args;
    timer_args.callback              = timer_callback;
    timer_args.arg                   = callback_arg;
    timer_args.dispatch_method       = ESP_TIMER_TASK;
    timer_args.name                  = "SampleTimer";
    timer_args.skip_unhandled_events = true;

    esp_timer_handle_t timer_handle;

    const auto err = esp_timer_create(&timer_args, &timer_handle);
    ESP_ERROR_CHECK(err);

    return timer_handle;
}

ReaderBase::ReaderBase(SampleBuffer<std::uint16_t> &aggregator)
  : _aggregator(aggregator)
  , _timer(make_timer(this))
{}

void ReaderBase::start_reading()
{
    setup_devices();
    CM_LOG_INFO_F("Starting sampling timer with interval {} microseconds...", _interval.count());

    const auto err =
        esp_timer_start_periodic(_timer, static_cast<std::uint64_t>(_interval.count()));
    ESP_ERROR_CHECK(err);
}

void ReaderBase::set_interval(std::chrono::microseconds interval)
{
    _interval = interval;
    if (esp_timer_is_active(_timer))
    {
        CM_LOG_INFO_F("Resetting timer with new interval of {} microseconds...", interval.count());

        esp_timer_stop(_timer);
        const auto err =
            esp_timer_start_periodic(_timer, static_cast<std::uint64_t>(interval.count()));
        ESP_ERROR_CHECK(err);
    }
}

void ReaderBase::make_new_data()
{
    if (uint16_t data; read_data(data))
    {
        aggregate_data(data);
    }
}

void ReaderBase::aggregate_data(std::uint16_t data)
{
    [[maybe_unused]] const auto _ = _aggregator.push_one(&data);
}
