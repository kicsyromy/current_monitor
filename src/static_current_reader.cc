#ifdef CONFIG_CM_SIMULATE_CURRENT_READINGS
#include <current_data_reader.hh>
#include "current_reader_constants.hh"

using namespace current_data;

StaticReader::StaticReader(Aggregator& aggregator)
    : ReaderBase(aggregator)
{
    CM_LOG_INFO("Using static reader");
}

bool StaticReader::read_data(std::uint16_t& out_data)
{
    out_data = constants::CURRENT_DATA[_index];
    _index = (_index + 1) % constants::CURRENT_DATA.size();
    return true;
}

StaticReader current_data::make_reader(Aggregator& aggregator)
{
    return StaticReader(aggregator);
}

#endif
