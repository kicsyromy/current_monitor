
The configuration message must be in the form of a header followed by a payload. The header has a fixed size of 48 bytes and the payload has a variable size up to 255 bytes.

HEADER + MESSAGE


Configuration header:

- magic: uint32_t - equals to 0xDEADBEEF
- payload_size: uint8_t - size of the message in payload
- message_type: uint8_t - type of the message in payload. Value 0 represents a DataPublisher config message and Value 1 represents a Statistics config message.

Statistics config message:

- pulse_threshold: uint16_t
- pulse_length: uint8_t

Data publisher config message:

- interval: uint8_t - represents the interval at which the data is computed for the Statistics collector and the interval at which data is sent for the Samples collector
- data_count: uint16_t - represents the maximum data to collect for the Statistics collector. Unused for the Samples collector.
- collector: uint8_t - represents the data collector to be used. Value 0 represents Statistics collector and value 1 represents Samples collector.
The configuration binary message must be published on the "/current_monitor/config/request" topic by the requester and the response will be published to the "/current_monitor/config/request" topic by the device.
The response to a configuration request message will be a string representing the result of the operation.
  
The possible result strings are:
- "InvalidHeaderSize" - used when the size of the received binary message is less than the size of the header (binary_message.size() < 48 bytes).
- "InvalidMagic" - used when the magic value received in the binary message differs from the expected value(0xDEADBEEF)
- "InvalidContentSize" - used when the size of the received binary message is less than the sum of the header size and the header payload size (binary_message.size() < (48 + payload_size))
- "InvalidType" - used when the message type field of the header contains an invalid value (message_type != 0 && message_type != 1)
- "Ok" - used when the message was successfully decoded and dispatched.

When the device receives a configuration it will update the current configuration and store the new configuration to the NVS. At the next reboot the device will try to read the configuration from the NVS and fallback to the compile defined configration in case a configuration is not present.
Data sampling:

The data is read every 100 microseconds from the sensor and is stored in a ring buffer from where a collector will consume it.
Data collectors:

### Statistics collector:

When the Statistics collector is used the data from the ring buffer will be computed at every 'interval' seconds and a maximum of 'data_count' iterations will be recorded until the data is published to MQTT.
The MQTT topic to which the data is published is "/current_monitor/stats/results".
The Statistics data which is recorded at every iteration consists of the pulse count and pulse average. The statistics computations use the configured 'pulse_threshold' and 'pulse_length' values.

### Samples collector:

When the Samples collector is used the data from the ring buffer is and published to MQTT at every 'interval' seconds.
The 'data_count' configuration is unused for this collector because all the available data from the ring buffer will be published to MQTT.
The MQTT topic to which the data is published is "/current_monitor/sample/results".
The data published by the Samples collector represents the raw readings collected every 100 microseconds from the sensor and can be very large.
When using this collector the maximum value for 'interval' can be 1 at the moment due to memory limitations.
