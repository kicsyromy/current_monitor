#!/usr/local/bin/bash


USER=$(whoami)
SDKCONFIG_FILE="sdkconfigs/$1"

source /Users/${USER}/esp/esp-idf/export.sh

if [ -z "$1" ]; then
    echo "No device EUI supplied"
    exit 1
fi

if [ -z $SDKCONFIG_FILE ]; then
    echo "No sdkconfig file for Device EUI: $1 supplied"
    exit 2
fi

idf.py build -DSDKCONFIG="${SDKCONFIG_FILE}"

scp build/current_monitor.bin staging-sensix:/sensix/firmware/binary/$1-pulse-counter-latest.bin

