#include <catch2/catch.hpp>

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wkeyword-macro"
#endif
#define private public
#include "statistics.cc"
#ifdef __clang__
#pragma clang diagnostic pop
#endif

#include <array>
#include <type_traits>

TEST_CASE("Case1", "statistics")
{
    static constexpr auto values = std::array{
        -2.00,  1.00,   10.00,  13.00,  25.00,  30.00,  49.00,  50.00,  51.00,  100.00,
        161.00, 161.00, 161.67, 162.17, 162.67, 163.17, 163.67, 164.17, 150.00, 100.00,
        90.00,  80.00,  70.00,  60.00,  50.00,  49.00,  20.00,  10.00,  1.00,   23.00,
        0.00,   -6.00,  -10.00, 20.00,  12.00,  3.00,   2.00,   3.00,   2.00,   2.00,
        1.80,   1.60,   1.40,   1.20,   1.00,   0.80,   0.60,   0.40,   0.20,   0.00,
        -0.20,  -0.40,  -0.60,  -0.80,  -1.00,  -1.20,  -1.40,  -1.60,  -1.80,  -2.00,
        -2.20,  -2.40,  -2.60,  -2.80,  -3.00,  -3.20,  -3.40,  -3.60,  -3.80,  -4.00,
        -4.20,  -4.40,  -4.60,  -4.80,  -5.00,  -5.20,  -5.40,  -5.60,  -5.80,  -6.00,
        -6.20,  -6.40,  -6.60,  -6.80,  -7.00,  -7.20,  -7.40,  -7.60,  -7.80,  -8.00,
        -8.20,  -8.40,  -8.60,  -8.80,  -9.00,  -9.20,  -9.40,  -9.60,  -9.80,  -10.00
    };

    auto values_raw = std::vector<std::uint16_t>{};
    values_raw.reserve(values.size());
    for (const auto &v : values)
    {
        values_raw.push_back(v < 0. ? 0 : static_cast<std::uint16_t>(v * 100));
    }

    const auto config     = Statistics::Config{ { Statistics::Config::DEFAULT_PULSE_THRESHOLD * 100,
                                              Statistics::Config::DEFAULT_PULSE_LENGTH } };
    auto       statistics = Statistics::compute_raw(values_raw, config);

    REQUIRE(statistics.pulse_count_ == 1);
    REQUIRE(statistics.pulse_amplitude_average_ == 12502);
    REQUIRE(statistics.pulse_width_average_ == 16);
}

TEST_CASE("Case2", "statistics")
{
    static constexpr auto values = std::array{
        -2.00,  1.00,   10.00,  13.00,  16.00,  25.00,  50.00,  51.00,  100.00, 140.00,
        161.00, 161.00, 161.67, 162.17, 162.67, 163.17, 163.67, 164.17, 150.00, 100.00,
        90.00,  80.00,  70.00,  60.00,  50.00,  49.00,  20.00,  10.00,  1.00,   -2.00,
        1.00,   10.00,  13.00,  16.00,  25.00,  50.00,  51.00,  100.00, 140.00, 140.00,
        140.00, 140.00, 140.00, 140.00, 140.00, 140.00, 140.00, 150.00, 100.00, 90.00,
        80.00,  70.00,  60.00,  50.00,  49.00,  20.00,  10.00,  -1.60,  -1.80,  -2.00,
        -2.20,  -2.40,  -2.60,  -2.80,  -3.00,  -3.20,  -3.40,  -3.60,  -3.80,  -4.00,
        -4.20,  -4.40,  -4.60,  -4.80,  -5.00,  -5.20,  -5.40,  20.00,  40.00,  49.99,
        50.00,  50.01,  52.00,  52.00,  53.00,  53.00,  53.30,  53.10,  52.10,  51.10,
        51.00,  50.00,  50.00,  49.99,  25.00,  10.00,  1.00,   -9.60,  -9.80,  -10.00
    };

    auto values_raw = std::vector<std::uint16_t>{};
    values_raw.reserve(values.size());
    for (const auto &v : values)
    {
        values_raw.push_back(v < 0. ? 0 : static_cast<std::uint16_t>(v * 100));
    }

    const auto config     = Statistics::Config{ { Statistics::Config::DEFAULT_PULSE_THRESHOLD * 100,
                                              Statistics::Config::DEFAULT_PULSE_LENGTH } };
    auto       statistics = Statistics::compute_raw(values_raw, config);

    REQUIRE(statistics.pulse_count_ == 3);
    REQUIRE(statistics.pulse_amplitude_average_ == 10504);
    REQUIRE(statistics.pulse_width_average_ == 14);
}

TEST_CASE("Case3", "statistics")
{
    static constexpr auto values = std::array{
        -2.00,  1.00,   10.00,  13.00,  16.00,  25.00,  50.00,  51.00,  100.00, 140.00,
        161.00, 161.00, 161.67, 162.17, 162.67, 163.17, 163.67, 164.17, 164.67, 165.17,
        165.67, 166.17, 166.67, 167.17, 167.67, 168.17, 168.67, 169.17, 169.67, 170.17,
        170.67, 171.17, 171.67, 172.17, 172.67, 173.17, 173.67, 174.17, 174.67, 175.17,
        175.67, 176.17, 176.67, 177.17, 177.67, 178.17, 178.67, 179.17, 179.67, 180.17,
        180.67, 181.17, 181.67, 182.17, 182.67, 183.17, 183.67, 184.17, 184.67, 185.17,
        185.67, 186.17, 186.67, 187.17, 187.67, 188.17, 188.67, 189.17, 189.67, 190.17,
        190.67, 191.17, 191.67, 192.17, 192.67, 193.17, 193.67, 194.17, 194.67, 195.17,
        195.67, 196.17, 180.00, 160.00, 120.00, 100.00, 51.00,  50.00,  51.00,  50.00,
        49.00,  48.00,  30.00,  20.00,  -9.00,  -9.20,  -9.40,  -9.60,  -9.80,  -10.00
    };

    auto values_raw = std::vector<std::uint16_t>{};
    values_raw.reserve(values.size());
    for (const auto &v : values)
    {
        values_raw.push_back(v < 0. ? 0 : static_cast<std::uint16_t>(v * 100));
    }

    const auto config     = Statistics::Config{ { Statistics::Config::DEFAULT_PULSE_THRESHOLD * 100,
                                              Statistics::Config::DEFAULT_PULSE_LENGTH } };
    auto       statistics = Statistics::compute_raw(values_raw, config);

    REQUIRE(statistics.pulse_count_ == 1);
    REQUIRE(statistics.pulse_amplitude_average_ == 17185);
    REQUIRE(statistics.pulse_width_average_ == 80);
}

TEST_CASE("Case4", "statistics")
{
    static constexpr auto values = std::array{
        9.02,  9.02,  9.02,  9.02,   9.02,   9.02,   9.02,  1.27,  1.27,  7.05,  1.27, 7.05, 1.55,
        10.55, 12.66, 25.00, 26.00,  31.00,  9.02,   1.27,  7.05,  1.27,  7.05,  1.27, 1.56, 1.56,
        33.00, 7.05,  55.55, 55.55,  55.55,  20.00,  40.00, 49.99, 49.00, 20.00, 7.04, 7.05, 7.04,
        1.27,  1.55,  1.55,  4.98,   1.00,   7.04,   12.00, 7.04,  7.04,  4.98,  4.98, 2.39, 7.05,
        2.39,  4.98,  4.98,  1.56,   2.39,   111.00, 2.39,  9.16,  9.16,  9.16,  1.56, 1.85, 1.85,
        1.85,  2.39,  0.97,  9.16,   7.04,   7.04,   0.97,  4.98,  9.16,  0.97,  1.85, 7.18, 4.98,
        7.18,  2.38,  1.85,  2.39,   5.29,   5.29,   0.97,  9.16,  9.16,  0.97,  1.72, 1.72, 7.18,
        7.18,  7.18,  1.85,  988.00, 566.00, 9.42,   9.42,  1.85,  5.29
    };

    auto values_raw = std::vector<std::uint16_t>{};
    values_raw.reserve(values.size());
    for (const auto &v : values)
    {
        values_raw.push_back(v < 0. ? 0 : static_cast<std::uint16_t>(v * 100));
    }

    const auto config =
        Statistics::Config{ { Statistics::Config::DEFAULT_PULSE_THRESHOLD * 100, 3 } };
    auto statistics = Statistics::compute_raw(values_raw, config);

    REQUIRE(statistics.pulse_count_ == 0);
    REQUIRE(statistics.pulse_amplitude_average_ == 0);
    REQUIRE(statistics.pulse_width_average_ == 0);
}
