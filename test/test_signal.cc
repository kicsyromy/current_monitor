#include <catch2/catch.hpp>

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wkeyword-macro"
#endif
#define private public
#include "signal.hh"
#ifdef __clang__
#pragma clang diagnostic pop
#endif

namespace
{
    constexpr auto EXPECTED_INT_VALUE = int{ 5 };

    auto slot_called = false;

    struct S
    {
        constexpr explicit S(int data_value = 0)
          : data{ data_value }
        {}

        inline ~S() = default;

        S(const S &other)
          : data{ other.data }
        {
            ++copy_construct_count;
        }

        S(S &&other)
        noexcept
          : data{ other.data }
        {
            ++move_construct_count;
            other.data = 0;
        }

        S &operator=(const S &lhs)
        {
            ++copy_assignment_count;

            data = lhs.data;

            return *this;
        }

        S &operator=(S &&lhs) noexcept
        {
            ++move_assignment_count;

            data = lhs.data;
            lhs.data = 0;

            return *this;
        }

        int data{ 0 };

        inline static std::size_t move_construct_count{ 0 };
        inline static std::size_t move_assignment_count{ 0 };
        inline static std::size_t copy_construct_count{ 0 };
        inline static std::size_t copy_assignment_count{ 0 };

        static void reset_counts() noexcept
        {
            move_construct_count = 0;
            move_assignment_count = 0;
            copy_construct_count = 0;
            copy_assignment_count = 0;
        }
    };
} // namespace

TEST_CASE("Signal<int>::connect - member function", "[signal]")
{
    auto signal = Signal<int>{};

    struct Test
    {
        void slot(void *, int value)
        {
            slot_called = true;
            REQUIRE(value == EXPECTED_INT_VALUE);
        }

        bool slot_called{ false };
    };

    auto test_object = Test{};

    signal.connect<&Test::slot>(test_object);
    signal.emit(nullptr, EXPECTED_INT_VALUE);

    REQUIRE(test_object.slot_called == true);
}

void test_signal_connection(void *, int value)
{
    slot_called = true;
    REQUIRE(value == EXPECTED_INT_VALUE);
}

TEST_CASE("Signal<int>::connect - free function", "[signal]")
{
    slot_called = false;

    auto signal = Signal<int>{};
    signal.connect(&test_signal_connection);
    signal.emit(nullptr, EXPECTED_INT_VALUE);

    REQUIRE(slot_called == true);
}

TEST_CASE("Signal<int, State>::connect - stateless lambda", "[signal]")
{
    struct State
    {
        bool slot_called{ false };
    };
    auto state = State{};

    auto signal = Signal<int>{};
    signal.connect([](void *sender, int value) {
        auto s = static_cast<State *>(sender);
        REQUIRE(value == EXPECTED_INT_VALUE);
        s->slot_called = true;
    });
    signal.emit(&state, EXPECTED_INT_VALUE);

    REQUIRE(state.slot_called == true);
}

TEST_CASE("Signal<S>::connect - member function", "[signal]")
{
    S::reset_counts();

    auto signal = Signal<S>{};

    struct Test
    {
        void slot(void *, S s)
        {
            slot_called = true;
            REQUIRE(s.data == EXPECTED_INT_VALUE);
        }

        bool slot_called{ false };
    };

    auto test_object = Test{};

    signal.connect<&Test::slot>(test_object);
    signal.emit(nullptr, S{ EXPECTED_INT_VALUE });

    REQUIRE(test_object.slot_called == true);

    REQUIRE(S::move_construct_count == 2);
    REQUIRE(S::move_assignment_count == 0);
    REQUIRE(S::copy_construct_count == 0);
    REQUIRE(S::copy_assignment_count == 0);
}

TEST_CASE("Signal<S&>::connect - member function", "[signal]")
{
    static void *s_address = nullptr;
    S::reset_counts();

    auto signal = Signal<S &>{};

    auto s = S{};
    s_address = &s;

    struct Test
    {
        void slot(void *, S &s)
        {
            slot_called = true;
            REQUIRE(s_address == &s);
        }

        bool slot_called{ false };
    };

    auto test_object = Test{};

    signal.connect<&Test::slot>(test_object);
    signal.emit(nullptr, s);

    REQUIRE(test_object.slot_called == true);

    REQUIRE(S::move_construct_count == 0);
    REQUIRE(S::move_assignment_count == 0);
    REQUIRE(S::copy_construct_count == 0);
    REQUIRE(S::copy_assignment_count == 0);
}

TEST_CASE("Signal<const S&>::connect - member function", "[signal]")
{
    static const void *s_address = nullptr;
    S::reset_counts();

    auto signal = Signal<const S &>{};

    const auto s = S{};
    s_address = &s;

    struct Test
    {
        void slot(void *, const S &s)
        {
            slot_called = true;
            REQUIRE(s_address == &s);
        }

        bool slot_called{ false };
    };

    auto test_object = Test{};

    signal.connect<&Test::slot>(test_object);
    signal.emit(nullptr, s);

    REQUIRE(test_object.slot_called == true);

    REQUIRE(S::move_construct_count == 0);
    REQUIRE(S::move_assignment_count == 0);
    REQUIRE(S::copy_construct_count == 0);
    REQUIRE(S::copy_assignment_count == 0);
}

TEST_CASE("Signal<S&&>::connect - member function", "[signal]")
{
    S::reset_counts();

    auto signal = Signal<S &&>{};

    struct Test
    {
        void slot(void *, S &&s)
        {
            slot_called = true;
            REQUIRE(s.data == EXPECTED_INT_VALUE);
        }

        bool slot_called{ false };
    };

    auto test_object = Test{};

    signal.connect<&Test::slot>(test_object);
    signal.emit(nullptr, S{ EXPECTED_INT_VALUE });

    REQUIRE(test_object.slot_called == true);

    REQUIRE(S::move_construct_count == 0);
    REQUIRE(S::move_assignment_count == 0);
    REQUIRE(S::copy_construct_count == 0);
    REQUIRE(S::copy_assignment_count == 0);
}

TEST_CASE("Signal<>::emit", "[signal]")
{
    /* Emit with no connection made */
    {
        Signal<> signal{};
        signal.emit(nullptr);
    }
}
