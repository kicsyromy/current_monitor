#include <catch2/catch.hpp>

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wkeyword-macro"
#endif
#define private public
#include "logger.hh"
#ifdef __clang__
#pragma clang diagnostic pop
#endif

#include <string_view>

TEST_CASE("logger::construct_format_specifier", "[logger]")
{
    constexpr auto fmt_specifier =
        Logger::construct_format_specifier("/path/to/test/source/file.cc", "111", "{:x}");

    REQUIRE(std::string_view{ fmt_specifier.data() } == std::string_view{ "file.cc:111: {:x}" });
}