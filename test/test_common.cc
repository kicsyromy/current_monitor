#include <catch2/catch.hpp>

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wkeyword-macro"
#endif
#define private public
#include "common.hh"
#ifdef __clang__
#pragma clang diagnostic pop
#endif

TEST_CASE("utility::get_file_name_offset", "[common]")
{
    constexpr auto offset = utility::get_file_name_offset("/path/to/some/source/file.cc");
    REQUIRE(offset == 21);
}
