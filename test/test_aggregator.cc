#include <catch2/catch.hpp>

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wkeyword-macro"
#endif
#define private public
#include "aggregator.cc"
#ifdef __clang__
#pragma clang diagnostic pop
#endif

TEST_CASE("Aggregator::add_value", "[Aggregator]")
{
    auto aggregator = Aggregator{};

    for (std::uint32_t i = 1; i < static_cast<std::uint32_t>(RINGBUF_CAPACITY_BYTES * 2); ++i)
    {
        REQUIRE(aggregator.add_value(Data::from(static_cast<std::uint16_t>(i)), i * 2));
        REQUIRE(aggregator.last_timestamp_ == i * 2);
    }
}

TEST_CASE("Aggregator::snapshot", "[Aggregator]")
{
    constexpr auto offset = std::uint32_t{ 256 };

    {
        auto aggregator = Aggregator{};

        for (std::uint32_t i = offset;
             i < static_cast<std::uint32_t>(RINGBUF_CAPACITY_BYTES / sizeof(Data)) + offset;
             ++i)
        {
            REQUIRE(aggregator.add_value(Data::from(static_cast<std::uint16_t>(i)), (i + 1) * 2));
        }

        const auto snapshot = aggregator.snapshot();
        for (std::size_t i = 0; i < snapshot.size(); ++i)
        {
            const auto delta = snapshot[i].delta;
            const auto value = snapshot[i].to_uint16();
            REQUIRE(value == (i + offset));
            REQUIRE(delta == 2);
        }
    }

    {
        auto aggregator = Aggregator{};

        for (std::uint32_t i = offset;
             i < static_cast<std::uint32_t>(RINGBUF_CAPACITY_BYTES / sizeof(Data)) + offset;
             ++i)
        {
            REQUIRE(aggregator.add_value(Data::from(static_cast<std::int16_t>(-i)), (i + 1) * 2));
        }

        const auto snapshot = aggregator.snapshot();
        for (std::size_t i = 0; i < snapshot.size(); ++i)
        {
            const auto delta = snapshot[i].delta;
            const auto value = snapshot[i].to_int16();
            REQUIRE(value == -(static_cast<std::int16_t>(i + offset)));
            REQUIRE(delta == 2);
        }
    }
}
