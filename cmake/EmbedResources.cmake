function (embed_resources)
    set (
        oneValueArgs
        TARGET
    )

    set (
        multiValueArgs
        RESOURCES
    )

    cmake_parse_arguments (
        CM "" "${oneValueArgs}" "${multiValueArgs}" ${ARGN}
    )
    
    if (CM_ARCH STREQUAL "xtensa")
        foreach (resource IN LISTS CM_RESOURCES)
            target_add_binary_data(${CM_TARGET} "${resource}" TEXT)
        endforeach ()
    endif ()
endfunction ()
